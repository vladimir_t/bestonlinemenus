<?php
$hookfile = '../.hooks/gitlab-webhook-push.sh';

$logfile = '../.hooks/gitlab-webhook-push.log';

$password = 'v1l2a3d4i5m6i5r7';


// THE ACTUAL SCRIPT
// -----------------
// You shouldn't edit beyond this point,
// unless you know what you're doing
// =====================================
function log_append($message, $time = null)
{
    global $logfile;

    $time = $time === null ? time() : $time;
    $date = date('Y-m-d H:i:s');
    $pre  = $date . ' (' . $_SERVER['REMOTE_ADDR'] . '): ';

    file_put_contents($logfile, $pre . $message . "\n", FILE_APPEND);
}

function exec_command($command)
{
    $output = array();

    exec($command, $output);

    foreach ($output as $line) {
        log_append('SHELL: ' . $line);
    }
}

if (isset($password))
{
    if (empty($_REQUEST['p'])) {
        log_append('Missing hook password');
        die();
    }

    if ($_REQUEST['p'] !== $password) {
        log_append('Invalid hook password');
        die();
    }
}

// GitLab sends the json as raw post data
$input = file_get_contents("php://input");
$json  = json_decode($input);

if (!is_object($json) || empty($json->ref)) {
    log_append('Invalid push event data');
    die();
}

if (isset($ref))
{
    $_refs = (array) $ref;

    if ($ref !== '*' && !in_array($json->ref, $_refs)) {
        log_append('Ignoring ref ' . $json->ref);
        die();
    }
}

log_append('Launching shell hook script...');
exec_command('sh ' . $hookfile);
log_append('Shell hook script finished');