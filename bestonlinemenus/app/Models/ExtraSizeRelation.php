<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExtraSizeRelation extends Model
{
    protected $_table = 'extra_size_relation';

    /**
     * Function to save size to extra relations
     *
     * @param $extra_id
     * @param $size_data
     * @param $item_id
     * @return bool
     */
    public function saveExtraSizeRelation($extra_id, $size_data, $item_id){
        if(!$extra_id || empty($size_data || !$item_id)){
            return false;
        }

        $rows_data = [];
        foreach ($size_data as $size_id){
            array_push($rows_data, ['extra_id' => $extra_id, 'size_id' => $size_id, 'item_id' => $item_id]);
        }

        return ModelHelper::insertOrUpdate($this->_table, $rows_data);
    }

    /**
     * Function to get list of sizes
     *
     * @param $extra_id
     * @param $item_id
     * @return array|bool
     */
    public function getSizesByExtraId($extra_id, $item_id){
        if(!$extra_id && !$item_id){
            return [];
        }

        $result = [];

        $query = DB::table($this->_table)
            ->select('size_id')
            ->where('extra_id', $extra_id)
            ->where('item_id', $item_id)
            ->get()
            ->toArray();


        foreach ($query as $row){
            array_push($result, $row->size_id);
        }

        if(!empty($result)){
            return $result;
        }

        return [];
    }

    /**
     * Function to remove all relations by item_id
     *
     * @param $item_id
     * @return bool|int
     */
    public function removeSizeRelationsByItemId($item_id){
        if(!$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->where('item_id', $item_id)
            ->delete();
    }
}
