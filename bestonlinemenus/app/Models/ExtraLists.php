<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExtraLists extends Model
{
    protected $_table = 'extra_lists';

    /**
     * Insert or update rows
     *
     * @param array $data
     * @return mixed
     */
    function insertOrUpdateExtraList(array $data){
        if(empty($data)){
           return false;
        }

        if(isset($data['id']) && $data['id'] != 0){
            $id = $data['id'];
            unset($data['id']);

            try{
               DB::table($this->_table)
                   ->where('id', $id)
                   ->update($data);

               return $id;
            } catch (Exception $e) {
               Log::error($e->getMessage(),  [
                   'code' => $e->getCode(),
               ]);
            }
        }else{
            try{
               $insert_id = DB::table($this->_table)
                   ->insertGetId($data);

               return $insert_id;
            } catch (Exception $e) {
               Log::error($e->getMessage(),  [
                   'code' => $e->getCode(),
               ]);
            }
        }
    }

    /**
     * Function to delete lists by ID
     *
     * @param array $list_ids
     * @return bool|int
     */
    public function deleteExtraListsById(array $list_ids){
        if(empty($list_ids)){
            return false;
        }

        return DB::table($this->_table)
            ->whereIn('id', $list_ids)
            ->delete();
    }

    /**
     * Function to get lists by category ID
     *
     * @param $section_id
     * @return array|bool
     */
    public function getListsByCategoryID($section_id){
        if(!$section_id){
            return false;
        }

        return DB::table($this->_table)
            ->where('section_id', $section_id)
            ->get()
            ->toArray();
    }
}
