<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Extra extends Model
{
    protected $_table = 'extra';
    /**
     * Insert or update rows
     *
     * @param int $section_id
     * @param int $item_id
     * @param array $rows
     * @return mixed
     */
    function insertOrUpdateExtra(array $rows, $section_id = 0, $item_id = 0){
        $insert_rows_data = [];
        $update_rows_data = [];
        $tmp_row = [];
        foreach ($rows as $key => $row){
            if(isset($row['id'])){
                $tmp_row['id'] = $row['id'];
            }

            if($section_id) {
                $tmp_row['section_id'] = $section_id;
            }

            if(isset($row['list_id'])){
                if($row['list_id'] == 0){
                    $tmp_row['list_id'] = null;
                }else {
                    $tmp_row['list_id'] = $row['list_id'];
                }
            }else{
                $tmp_row['list_id'] = null;
            }

            if($item_id) {
                $tmp_row['item_id'] = $item_id;
            }

            $tmp_row['name'] = $row['name'];
            $tmp_row['price'] = $row['price'];

            if(isset($row['size_id'])){
                $tmp_row['size_id'] = $row['size_id'];
            }

            if(isset($row['id']) && $row['id']){
                array_push($update_rows_data, $tmp_row);
            }else{
                array_push($insert_rows_data, $tmp_row);
            }
        }

        if(!empty($update_rows_data)){
            ModelHelper::multipleUpdate($this->_table, $update_rows_data, 'id');
        }

        if(!empty($insert_rows_data)) {
            return ModelHelper::insertOrUpdate($this->_table, $insert_rows_data);
        }

        return false;
    }


    /**
     * Function get extra items by menu item ID
     *
     * @param $item_id
     * @return mixed
     */
    function getAllByItemId($item_id){
        if(!$item_id){
            return false;
        }

        $query =  DB::table('extra_item_relation as eir')
            ->select('e.id', 'e.name', 'e.price', 'el.id as list_id', 'el.name as list_name', 'el.multi_selection')
            ->leftJoin('extra as e', 'e.id', '=', 'eir.extra_id')
            ->leftJoin('extra_lists as el', 'el.id', '=', 'e.list_id')
            ->where('eir.item_id', $item_id)
            ->get()
            ->toArray();

        foreach ($query as $key => $item) {
            $item->sizes = (new ExtraSizeRelation())->getSizesByExtraId($item->id, $item_id);
        }

        return $this->generateResponse($query, false, $item_id);
    }


    /**
     * Function get extra items by menu section ID
     *
     * @param $section_id
     * @return mixed
     */
    function getAllBySectionId($section_id){
        if(!$section_id){
            return false;
        }

        // Get all extras
        $query =  DB::table($this->_table . ' as e')
            ->select('e.id', 'e.name', 'e.price', 'el.id as list_id', 'el.name as list_name', 'el.multi_selection')
            ->leftJoin('extra_lists as el', 'el.id', '=', 'e.list_id')
            ->where('e.section_id', $section_id)
            ->orderBy('list_id', 'desc')
            ->get()
            ->toArray();

        return $this->generateResponse($query, $section_id);
    }

    /**
     * Function get extra items by list ID
     *
     * @param $list_id
     * @return mixed
     */
    function getAllByListId($list_id){
        if(!$list_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('id', 'name', 'price')
            ->where('list_id', $list_id)
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

    }

    /**
     * Function to get list of id's of extra in item
     *
     * @param $item_id
     * @return mixed
     */
    function getItemIdsList($item_id){
        if(!$item_id){
            return false;
        }

        return (new ExtraItemRelation())->getExtrasByItemID($item_id);
    }

    /**
     * Function to get list of id's of extra in category
     *
     * @param $cat_id
     * @return mixed
     */
    function getCategoryIdsList($cat_id){
        if(!$cat_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('id')
            ->where('section_id', $cat_id)
            ->get()
            ->toArray();

    }

    /**
     * Function to delete extra from item records by id
     *
     * @param array $ids_array
     * @param array $item_id
     * @return mixed
     *
     */
    function deleteItemsExtra($ids_array, $item_id){
        if(empty($ids_array) && !$item_id){
            return false;
        }

        return (new ExtraItemRelation())->deleteRelations($ids_array, $item_id);
    }

    /**
     * Function to delete extra records by id
     *
     * @param array $ids_array
     * @return mixed
     *
     */
    function deleteExtra($ids_array){
        if(empty($ids_array)){
            return false;
        }

        return DB::table($this->_table)
            ->whereIn('id', $ids_array)
            ->delete();
    }

    /**
     * Function to save extra data
     *
     * @param $data
     * @param $cat_id
     * @return bool
     */
    public function saveExtraData($data, $cat_id){
        if(empty($data) || !$cat_id){
            return false;
        }

        $extra_lists = [];
        if(isset($data['extra_lists'])){
            $extra_lists = $data['extra_lists'];
        }

        $extra_data = [];
        if(isset($data['extras'])){
            $extra_data = $data['extras'];
        }

        // Get current lists
        $get_current_lists = (new ExtraLists())->getListsByCategoryID($cat_id);
        // Get deleted rows id's
        $deleted_lists_rows = ModelHelper::getDeletedIDs($get_current_lists, $extra_lists);

        if (!empty($deleted_lists_rows)) {
            // Remove deleted rows
            (new ExtraLists())->deleteExtraListsById($deleted_lists_rows);
        }

        // Insert / Update Extra Lists
        if($extra_lists){
            foreach ($extra_lists as $data) {
                $extra_list_id = (new ExtraLists())->insertOrUpdateExtraList(['id' => (isset($data['id'])) ? $data['id'] : 0, 'section_id' => $cat_id, 'name' => $data['name'], 'multi_selection' => $data['multi_selection']]);

                if (!empty($data['items'])) {
                    foreach ($data['items'] as $item){
                        if(!isset($item['id'])){
                            $item['id'] = 0;
                        }
                        $item['list_id'] =  $extra_list_id;
                        array_push($extra_data, $item);
                    }
                }
            }
        }

        // Insert / Update Extra
        if($extra_data){
            $this->processExtraRows($cat_id,  $extra_data);
        }

        return $this->getAllBySectionId($cat_id);
    }

    /**
     * Function to process Extra rows
     *
     * @param $cat_id
     * @param $extra_data
     * @return boolean
     */
    public function processExtraRows($cat_id, $extra_data){
        if(!$cat_id || empty($extra_data)){
            return false;
        }

        $current_extra_rows = $this->getCategoryIdsList($cat_id);
        // Get deleted rows id's
        $deleted_extra_rows = ModelHelper::getDeletedIDs($current_extra_rows, $extra_data);

        if (!empty($deleted_extra_rows)) {
            // Remove deleted rows
            $this->deleteExtra($deleted_extra_rows);
        }

        $extra_rows = ModelHelper::generateExtraAndSizeArray($cat_id, $extra_data);
        if($extra_rows) {
            // Insert multiple rows
            $this->insertOrUpdateExtra($extra_rows, $cat_id);

            // Check if extra list has relation to item and if yes, add new extra to item relation
            foreach ($extra_rows as $row){
                if($row['id'] == 0 && isset($row['list_id'])) {
                    $items_ids = (new ExtraListItemRelation())->getItemIdsByListId($row['list_id']);
                    $extra_id = $this->getIdByNameAndCat($cat_id, $row['name']);
                    $new_extras = [];
                    if(!empty($items_ids)) {
                        foreach ($items_ids as $item_id) {
                            array_push($new_extras, ['extra_id' => $extra_id, 'item_id' => $item_id]);
                        }

                        if (!empty($new_extras)) {
                            (new ExtraItemRelation())->saveRelations($new_extras);
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Function to update extra rows
     *
     * @param $data
     * @param $item_id
     * @return mixed
     */
    public function updateExtra($data, $item_id){
        if(empty($data) || !$item_id){
            return false;
        }

        $extra_data = $data['extras'];

        $list_ids = [];
        if(isset($data['extra_lists'])){
            foreach ($data['extra_lists'] as $list){
                $list_id = $list['id'];
                array_push($list_ids, ['id' => $list_id, 'item_id' => $item_id]);
                foreach ($list['items'] as $item){
                    $item['list_id'] = $list_id;
                    array_push($extra_data, $item);
                }
            }
        }

        // Get current item lists
        $current_lists = (new ExtraListItemRelation())->getListsByItemID($item_id);

        // Get deleted list ids
        $delete_list_ids = ModelHelper::getDeletedIDs($current_lists, $list_ids, 'list_id');

        // Delete relations
        if(!empty($delete_list_ids)){
            (new ExtraListItemRelation())->deleteRelations($delete_list_ids, $item_id);
        }

        // Save lists relation
        if(!empty($list_ids)){
            (new ExtraListItemRelation())->saveRelations($list_ids);
        }

        $section_id = (new MenuItem())->getCatID($item_id);

        $current_extra_rows = $this->getItemIdsList($item_id);

        // Get deleted rows id's
        $deleted_extra_rows = ModelHelper::getDeletedIDs($current_extra_rows, $extra_data, 'extra_id');

        if(!empty($deleted_extra_rows)) {
            // Remove deleted rows
            $this->deleteItemsExtra($deleted_extra_rows, $item_id);
        }

        // Remove all size relations before update
        (new ExtraSizeRelation())->removeSizeRelationsByItemId($item_id);

        // Insert or Update
        $extra_item_relation = [];
        foreach ($extra_data as $key => $data){
            $data['section_id'] = $section_id;

            $sizes = [];
            if(isset($data['sizes'])){
                $sizes = $data['sizes'];
                unset($data['sizes']);
            }

            if(isset($data['id']) && $data['id'] != 0){
                $id = $data['id'];
                array_push($extra_item_relation, ['extra_id' => $id, 'item_id' => $item_id]);
                unset($data['id']);
                try {
                    DB::table($this->_table)
                        ->where('id', $id)
                        ->update($data);
                } catch (\Illuminate\Database\QueryException $e) {
                    Log::error($e->getMessage(),  [
                        'code' => $e->getCode(),
                    ]);
                }
            }else{
                try {
                    $id = DB::table($this->_table)
                        ->insertGetId($data);

                    array_push($extra_item_relation, ['extra_id' => $id, 'item_id' => $item_id]);
                } catch (\Illuminate\Database\QueryException $e) {
                    Log::error($e->getMessage(),  [
                        'code' => $e->getCode(),
                    ]);
                }
            }

            if(!empty($sizes)) {
                //Save size to extra relation
                (new ExtraSizeRelation())->saveExtraSizeRelation($id, $sizes, $item_id);
            }
        }

        if(!empty($extra_item_relation)){
            (new ExtraItemRelation())->saveRelations($extra_item_relation);
        }

        //Get updated item info
        $response = $this->getAllByItemId($item_id);

        return $response;
    }


    /**
     * Function to generate response array
     *
     * @param $data
     * @param $section_id
     * @param $item_id
     * @return mixed
     */
    function generateResponse($data, $section_id = false, $item_id = false){
        $result = [];

        if($section_id) {
            $result['category_id'] = $section_id;
        }

        if($item_id){
            $result['item_id'] = $item_id;
        }
        // Generate extras and lists arrays
        $extras = [];
        $extras_lists = [];
        $tmp_id = 0;
        foreach ($data as $row){
            if(!$row->list_id){
                $row_data = ['id' => $row->id, 'name' => $row->name, 'price' => $row->price];
                if($item_id){
                    $row_data['sizes'] = (isset($row->sizes)) ? $row->sizes : [];
                }
                array_push($extras, $row_data);
            }else{
                if($tmp_id != $row->list_id) {
                    array_push($extras_lists, ['id' => $row->list_id, 'name' => $row->list_name, 'multi_selection' => $row->multi_selection]);
                }

                $list_key = array_search($row->list_name, array_column($extras_lists, 'name'));
                if (isset($extras_lists[$list_key])) {
                    $row_data = ['id' => $row->id, 'name' => $row->name, 'price' => $row->price];
                    if($item_id){
                        $row_data['sizes'] = (isset($row->sizes)) ? $row->sizes : [];
                    }
                    $extras_lists[$list_key]['items'][] = $row_data;
                }
                $tmp_id = $row->list_id;
            }
        }

        $result['extra_lists'] = $extras_lists;
        $result['extras'] = $extras;

        if(!empty($result)){
            return $result;
        }

        return false;
    }

    /**
     * Function to get Extra ID by name.
     *
     * @param $cat_id
     * @param $name
     * @return bool|mixed
     */
    function getIdByNameAndCat($cat_id, $name){
        if(!$cat_id && !$name){
            return false;
        }

        $result = DB::table($this->_table)
            ->select('id')
            ->where('section_id', $cat_id)
            ->where('name', $name)
            ->first();

        return $result->id;

    }
}
