<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelHelper extends Model
{
    /**
     * Function to multiple insert / update rows.
     *
     * @param $table
     * @param array $rows
     * @return bool
     */
    static function insertOrUpdate($table, array $rows){
        $first = reset($rows);

        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );

        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                        array_map( function( $value ) { if($value == null){ return 'NULL'; } else { return '"'.str_replace('"', '""', $value).'"'; }} , $row )
                    ).')';
            } , $rows )
        );

        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );

        $sql = "INSERT INTO {$table} ({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

        return DB::statement( $sql );
    }

    /**
     * Function to multiple update rows by index.
     *
     *
     * @param $table
     * @param array $values
     * @param $index
     * @return bool
     */
    static function multipleUpdate($table, array $values, $index){
        $final = [];
        $ids = [];

        if (!count($values)) {
            return false;
        }
        if (!isset($index) && empty($index)) {
            return false;
        }

        foreach ($values as $key => $val) {
            $ids[] = $val[$index];
            foreach (array_keys($val) as $field) {
                if ($field !== $index) {
                    $value = (is_null($val[$field]) ? 'NULL' : '"' . self::mysql_escape($val[$field]) . '"');
                    $final[$field][] = 'WHEN `' . $index . '` = "' . $val[$index] . '" THEN ' . $value . ' ';
                }
            }
        }

        $cases = '';
        foreach ($final as $k => $v) {
            $cases .= '`' . $k . '` = (CASE ' . implode("\n", $v) . "\n"
                . 'ELSE `' . $k . '` END), ';
        }

        $query = "UPDATE `" . $table . "` SET " . substr($cases, 0, -2) . " WHERE `$index` IN(" . '"' . implode('","', $ids) . '"' . ");";

        return DB::statement( $query );
    }

    /**
     * Custom mysql_escape function.
     *
     * @param $inp
     * @return array|mixed
     */
    static function mysql_escape($inp)
    {
        if(is_array($inp)) return array_map(__METHOD__, $inp);

        if(!empty($inp) && is_string($inp))
        {
            return str_replace(
                ['\\', "\0", "\n", "\r", "'", '"', "\x1a"],
                ['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'],
                $inp);
        }

        return $inp;
    }

    /**
     * Function to generate Extra or Size rows array
     *
     * @param $section_id
     * @param $data
     * @return array|bool
     */
    static function generateExtraAndSizeArray($section_id, $data, $item_id = 0){
        if(!$section_id && empty($data)){
            return [];
        }

        $rows = [];
        foreach ($data as $item){
            $item_array = (array) $item;
            if(!empty($item_array)) {
                $row = ['id' => (isset($item_array['id'])) ? $item_array['id'] : 0, 'section_id' => $section_id, 'item_id' => (isset($item_array['item_id'])) ? $item_array['item_id'] : $item_id, 'name' => $item_array['name'], 'price' => ($item_array['price']) ? $item_array['price'] : 0];
                if(isset($item_array['list_id'])){
                    $row['list_id'] = $item_array['list_id'];
                }
                array_push($rows, $row);
            }
        }

        if(!empty($rows)){
            return $rows;
        }

        return false;
    }

    /**
     * Function to generate delete id's array for Extra/Size records.
     *
     * @param $array_current
     * @param $array_new
     * @param $index
     * @return array|bool
     */
    static function getDeletedIDs($array_current, $array_new, $index = false){
        $tmp_new_arr = [];
        foreach ($array_new as $new_values){
            if(isset($new_values['id'])){
                array_push($tmp_new_arr, $new_values['id']);
            }
        }

        $tmp_curr_arr = [];
        foreach ($array_current as $current_val){
            if(!$index) {
                if (isset($current_val->id)) {
                    array_push($tmp_curr_arr, $current_val->id);
                }
            }else{
                if (isset($current_val->$index)) {
                    array_push($tmp_curr_arr, $current_val->$index);
                }
            }
        }

        $delete_ids = [];
        if(!empty($tmp_curr_arr) && !empty($tmp_new_arr)) {
            $delete_ids = array_diff($tmp_curr_arr, $tmp_new_arr);
        }

        if(!empty($array_current) && empty($tmp_new_arr) && empty($delete_ids)){
            $delete_ids = $tmp_curr_arr;
        }

        return $delete_ids;
    }
}