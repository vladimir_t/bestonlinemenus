<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExtraListItemRelation extends Model
{
    protected $_table = 'extra_list_item_relation';

    /**
     * Function to save multiple relations
     *
     * @param $relations
     * @return mixed
     */
    public function saveRelations($relations){
        if(empty($relations)){
            return false;
        }

        $rows = [];

        // Changing key ID to LIST_ID
        foreach ($relations as $relation){
            array_push($rows, ['list_id' => $relation['id'], 'item_id' => $relation['item_id']]);
        }

        return ModelHelper::insertOrUpdate($this->_table, $rows);
    }

    /**
     * Function to delete relations by list ids
     *
     * @param $ids_array
     * @param $item_id
     * @return bool|int
     */
    public function deleteRelations($ids_array, $item_id){
        if(empty($ids_array) && !$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->whereIn('list_id', $ids_array)
            ->where('item_id', $item_id)
            ->delete();
    }


    /**
     * Function to get related lists ids by item id.
     *
     * @param $item_id
     * @return array|bool
     */
    public function getListsByItemID($item_id){
        if(!$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('list_id')
            ->where('item_id', $item_id)
            ->get()
            ->toArray();
    }

    /**
     * Function to get list of item ids by list ID.
     *
     * @param $list_id
     * @return array|bool
     */
    public function getItemIdsByListId($list_id){
        if(!$list_id){
            return false;
        }

        $result = [];

        $query = DB::table($this->_table)
            ->where('list_id', $list_id)
            ->get()
            ->toArray();


        foreach ($query as $row){
            array_push($result, $row->item_id);
        }

        return $result;
    }
}
