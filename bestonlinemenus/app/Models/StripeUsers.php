<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class StripeUsers extends Model
{
    /**
     * Save stripe user connection
     *
     * @param $restaurant_id
     * @param $data
     * @return bool
     */
    public function saveAction($restaurant_id, $data){
        if(empty($data)){
            return false;
        }

        $update_data = [];
        if(isset($data->stripe_user_id)){
            $update_data['stripe_user_id'] = $data->stripe_user_id;
        }

        if(isset($data->stripe_publishable_key)){
            $update_data['stripe_publishable_key'] = $data->stripe_publishable_key;
        }

        if(isset($data->access_token)){
            $update_data['access_token'] = $data->access_token;
        }

        if(isset($data->refresh_token)){
            $update_data['refresh_token'] = $data->refresh_token;
        }

        //Check if we have record with the same restaurant
        $result = DB::table('stripe_users')
            ->where('restaurant_id', $restaurant_id)
            ->first();

        if(isset($result->id)){
            $update_data['updated_at'] = date('Y-m-d h:i:s a', time());
            try {
                DB::table('stripe_users')
                    ->where('id',  $result->id)
                    ->update($update_data);

                return true;
            }catch (QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else{
            $update_data['created_at'] = date('Y-m-d h:i:s a', time());
            if(!isset($update_data['restaurant_id'])){
                $update_data['restaurant_id'] = $restaurant_id;
            }

            try{
                DB::table('stripe_users')
                    ->insert($update_data);

                return true;
            }catch (QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }

        return false;
    }

    /**
     * Get Stripe account ID by restaurant id.
     *
     * @param $restaurant_id
     * @return bool|mixed
     */
    public function getAccountID($restaurant_id){
        if(!$restaurant_id){
            return false;
        }

        $result = DB::table('stripe_users')
            ->where('restaurant_id', $restaurant_id)
            ->first();

        if($result){
            return $result->stripe_user_id;
        }

        return false;
    }

    /**
     * Get Stripe publish key by restaurant id
     *
     * @param $restaurant_id
     * @return bool|mixed
     *
     */
    public function getPublishKey($restaurant_id){
        if(!$restaurant_id){
            return false;
        }

        $result = DB::table('stripe_users')
            ->where('restaurant_id', $restaurant_id)
            ->first();

        if($result){
            return $result->stripe_publishable_key;
        }

        return false;
    }

    /**
     * Delete Stripe connection record
     *
     * @param $restaurant_id
     * @return bool
     */
    public function deleteAction($restaurant_id){
        if(!$restaurant_id){
            return false;
        }

        try {
            DB::table('stripe_users')
                ->where('restaurant_id', $restaurant_id)
                ->delete();

            return true;
        }catch (QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }

    /** Function to check if profile is connected */
    public function isConnected($restaurant_id){
        if(!$restaurant_id){
            return false;
        }

        $result = DB::table('stripe_users')
            ->select('id')
            ->where('restaurant_id', $restaurant_id)
            ->first();

        if($result){
            return true;
        }

        return false;
    }
}
