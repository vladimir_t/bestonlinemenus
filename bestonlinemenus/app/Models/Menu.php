<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Menu extends Model
{
    /**
     * Function to get list of all menus
     *
     * @return array
     */
    public function getAll(){
        return DB::table('menus')
            ->get()
            ->toArray();
    }


    /**
     * Function to get menus list by Restaurant ID
     *
     * @param $user_id
     * @return mixed
     */
    public function getMenusByRestaurantId($user_id){
        if(!$user_id){
            return false;
        }

        return DB::table('menus')
            ->where('restaurant_id', $user_id)
            ->get()
            ->toArray();
    }

    /**
     * Function to get single menu ID by Restaurant ID
     *
     * @param $user_id
     * @return mixed
     */
    public function getMenuByIdRestaurantId($user_id){
        if(!$user_id){
            return false;
        }

        return DB::table('menus')
            ->select('id')
            ->where('restaurant_id', $user_id)
            ->get()
            ->first();
    }

    /**
     * Function to get menu by ID
     *
     * @param $id
     * @return mixed
     */
    public function getMenuById($id){
        return $result = DB::table('menus as m')
            ->select('m.*', 'r.id as rest_id')
            ->where('m.id', $id)
            ->leftJoin('restaurant_profile as r', 'm.id', '=', 'r.menu_id')
            ->get()
            ->first();
    }

    /**
     * Function to save menu
     *
     * @param $id
     * @param $data
     * @return bool|string
     */
    public function saveMenu($id, $data){
        if($id != 0){
            try{
                DB::table('menus')
                    ->where('id', $id)
                    ->update($data);

                return true;
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else{
            try {
                DB::table('menus')
                    ->insert($data);

                return true;
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }
    }


    /**
     * Function to delete menu
     *
     * @param $id
     * @return bool|string
     */
    public function deleteMenu($id){
        if(!$id){
            return false;
        }

        try{
            DB::table('menus')
                ->where('id', $id)
                ->delete();

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }
    }
}
