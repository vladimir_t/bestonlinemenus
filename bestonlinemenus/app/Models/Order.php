<?php

namespace App\Models;

use App\Http\Controllers\StripeController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    /**
     * Function to save order info
     *
     * @param $data
     * @return array
     */
    public function saveOrderInfo($data){
        if(empty($data)){
            return false;
        }

        if(!isset($data['order'])){
            abort(422, 'Order info is required!');
        }

        // set order products into tmp variable
        $order_info = $data['order'];
        unset($data['order']);

        // convert cent to dollar
        if(isset($data['total_price'])){
            $data['total_price'] = money_format('%i', ($data['total_price'] / 100));
        }

        try{
            //save order table
            $order_id = DB::table('order')
                ->insertGetId($data);

            // Save order product info
            (new OrderProducts())->saveOrderProduct($order_id, $order_info);

            return ['status' => 'created', 'order_id' => $order_id];
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }

    /**
     * Function to update order info
     *
     * @param $data
     * @return bool
     */
    public function updateOrderClientInfo($id, $data){
        if(empty($data)){
            return false;
        }

        if(!$id){
            abort(422, 'Order id is required!');
        }

        $data['date_updated'] = date('Y-m-d h:i:s a', time());

        try {
            //update order table
            DB::table('order')
                ->where('id', $id)
                ->update($data);

            return $this->getOrderById($id);
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }
    }

    /**
     * Function to get Orders info
     *
     * @param $restaurant_id
     * @param bool $status
     * @param bool $byDate
     * @param int $limit
     * @return bool
     */
    public function getOrders($data, $isCopmleted = false){
        if(!$data['restaurant_id']){
            return false;
        }

        $orders = [
            'data' => [],
            'total' => 0
        ];

        $restaurant_id = $data['restaurant_id'];

        $where = [
            'o.restaurant_id' => $restaurant_id
        ];

        if($isCopmleted && !isset($data['status'])){
            $where['o.status'] = 'complete';
        }elseif(isset($data['status'])){
            $where['o.status'] = $data['status'];
        }

        if(isset($data['date'])){
            $where['o.date_created'] = $data['date'];
        }

        if(isset($data['revenue'])){
            $where['o.total_price'] = $data['revenue'];
        }

        if(isset($data['start_date'])) {
            $where_search = ['o.date_updated', '>=', $data['start_date'] . ' 00:00:00'];
            array_push($where, $where_search);
        }

        if(isset($data['stop_date'])){
            $where_search = ['o.date_updated', '<=',  $data['stop_date'] . ' 23:59:59'];
            array_push($where, $where_search);
        }

        $limit = 10;
        if(isset($data['limit'])){
            $limit = $data['limit'];
        }

        $result = DB::table('order as o')
            ->select('o.*')
            ->where($where)
            ->orderBy('date_created', 'desc')
            ->get()
//            ->paginate($limit)
            ->toArray();

        if(!empty($result)){
            foreach ($result as $key=>$order){
                $products = DB::table('order_products as op')
                    ->select('op.id', 'op.dish_id', 'op.amount', 'mi.name as name', 'mi.price as price', 's.name as size', 's.price as size_price', 's.id as size_id')
                    ->leftJoin('menu_item as mi', 'mi.id', '=', 'op.dish_id')
                    ->leftJoin('size as s', 's.id', '=', 'op.selectedSize')
                    ->where('order_id', $order->id)
                    ->get();

                $products_arr = [];
                foreach ($products as $product){
                    $product->selectedSize = [
                        'id' => $product->size_id,
                        'name' => $product->size,
                        'price' => $product->size_price
                    ];
                    unset($product->size_id);
                    unset($product->size);
                    unset($product->size_price);

                    $product->selectedExtras = (new OrderProductsExtra())->getExtrasByProductID($product->id);
                    array_push($products_arr, $product);
                }

                if(!empty($products_arr)){
                    $result[$key]->products = $products_arr;
                }
            }

            $orders['data'] = $result;
            $orders['total'] = count($result);
        }

        return $orders;
    }

    /**
     * Get order info by ID
     *
     * @param $order_id
     * @return bool|Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getOrderById($order_id){
        if(!$order_id){
            return false;
        }

        // default result
        $result = [];

        $get_order_result = DB::table('order as o')
            ->select('o.*')
            ->where('o.id', $order_id)
            ->first();

        if(!empty($get_order_result)){
            $result = (array) $get_order_result;
            $products = DB::table('order_products as op')
                ->select('op.id', 'op.dish_id', 'op.amount', 'mi.name as name', 'mi.price as price', 's.name as size', 's.price as size_price', 's.id as size_id')
                ->leftJoin('menu_item as mi', 'mi.id', '=', 'op.dish_id')
                ->leftJoin('size as s', 's.id', '=', 'op.selectedSize')
                ->groupBy('op.id')
                ->where('order_id', $get_order_result->id)
                ->get();

            $products_arr = [];
            foreach ($products as $product){
                // Add selectedSize
                $product->selectedSize = [
                    'id' => $product->size_id,
                    'name' => $product->size,
                    'price' => $product->size_price
                ];
                unset($product->size_id);
                unset($product->size);
                unset($product->size_price);

                $product->selectedExtras = (new OrderProductsExtra())->getExtrasByProductID($product->id);
                array_push($products_arr, $product);
            }

            if(!empty($products_arr) &&  !empty($result)){
                $result['products'] = $products_arr;
            }

            return $result;
        }

        return false;
    }

    /**
     * Function to get restaurant revenue
     *
     * @param $restaurant_id
     * @return mixed|null
     */
    public function getTotalRevenue($restaurant_id){
        if(!$restaurant_id){
            return null;
        }

        $result = DB::table('order')
            ->where(['restaurant_id' => $restaurant_id, 'status' => 'complete'])
            ->sum('total_price');

        return money_format('%i', $result);
    }


    /**
     * Function to get all revenues values
     *
     * @return array
     */
    public function getAllRevenues(){
        $result = DB::table('restaurant_profile as rp')
            ->select(DB::raw('rp.id, (SELECT SUM(`o`.`total_price`) as `sum` FROM `order` as `o` WHERE `o`.`restaurant_id` = `rp`.`id` AND `o`.`status` = "complete") as revenue'))
            ->get();

        $revenues = [];
        $tmp_val = '';
        foreach ($result as $row){
            $value = $row->revenue;
            if(!$value){
                $value = 0;
            }

            $value = money_format('%i', $value);
            if($tmp_val != $value) {
                array_push($revenues, ['name' => '$' . $value, 'value' => $value]);
            }
            $tmp_val = $value;
        }

        return $revenues;
    }

    /**
     * Function to delete Order by ID
     *
     * @param $order_id
     * @return bool
     */
    public function deleteOrder($order_id){
        if(!$order_id){
            return false;
        }

        $order = DB::table('order')
            ->where('id', $order_id)
            ->first();

        // If payment method pay_now we should make refund for first
        if($order->payment_method == 'pay_now' && $order->transaction_id){
            (new StripeController())->doRefund($order->transaction_id, $order->total_price);
        }

        try{
            DB::table('order')
                ->where('id', $order_id)
                ->delete();

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }
}
