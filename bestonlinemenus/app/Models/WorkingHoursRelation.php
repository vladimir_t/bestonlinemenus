<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WorkingHoursRelation extends Model
{
    private $_table = 'working_hours_relation';

    /**
     * Insert or update rows
     *
     * @param array $rows
     * @return mixed
     */
    function insertOrUpdateHours(array $rows){
        return ModelHelper::insertOrUpdate($this->_table, $rows);
    }

    /**
     * Function to get Working hours by Restaurant ID
     *
     *
     * @param $restaurant_id
     * @return array|bool
     *
     */
    public function getHoursByRestaurantId($restaurant_id){
        if(!$restaurant_id){
            return false;
        }

        $result = DB::table($this->_table)
            ->where('restaurant_id', trim($restaurant_id))
            ->get()
            ->toArray();

        $result_array = [];
        foreach ($result as $item){
            $result_array[$item->type][$item->day] = [
                "start" => $item->start_date,
                "end" => $item->end_date,
                "isClosed" => ($item->isClosed === "true") ? true : false
            ];
        }

        if(!empty($result_array)){
            return $result_array;
        }

        return false;
    }
}
