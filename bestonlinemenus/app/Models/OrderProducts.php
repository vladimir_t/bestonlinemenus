<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderProducts extends Model
{
    private $_table = 'order_products';

    /**
     * Save order product function
     *
     * @param $order_id
     * @param $data
     * @return bool|mixed
     */
    public function saveOrderProduct($order_id, $data){
        if(!$order_id || empty($data)){
            return false;
        }

        $row = [];

        // Save product and extra
        foreach ($data as $product){
            $row['order_id'] = $order_id;
            $row['dish_id'] = $product['id'];
            $row['amount'] = 1;

            if(isset($product['selectedSize'])) {
                $row['selectedSize'] = $product['selectedSize'];
            }

            if(isset($product['amount'])){
                $row['amount'] = $product['amount'];
            }

            try {
                $product_id = DB::table($this->_table)
                    ->insertGetId($row);

                if(isset($product['selectedExtras']) && !empty($product['selectedExtras'])){
                    if(!(new OrderProductsExtra())->saveProductExtra($product_id, $product['selectedExtras'])){
                        continue;
                    }
                }else{
                    continue;
                }

                return true;
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }

    }

    /**
     * Function to update order by ID
     *
     * @param $order_id
     * @param $data
     * @return bool|mixed
     */
    public function updateProducts($order_id, $data, $total_price){
        if(!$order_id || empty($data)){
            return false;
        }

        try {
            //Delete all old records
            DB::table('order_products')
                ->where('order_id', $order_id)
                ->delete();

        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }


        // Save new records and update total price
        if($this->saveOrderProduct($order_id, $data)){
            //Update total order price
            try {
                DB::table('order')
                    ->where('id', $order_id)
                    ->update(['total_price' => $total_price]);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
            return (new Order())->getOrderById($order_id);
        }

        return false;
    }
}
