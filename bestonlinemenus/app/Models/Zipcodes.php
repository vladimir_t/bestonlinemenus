<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Zipcodes extends Model
{
    function getInfoByFilter($search_word, $limit = 10){
        if(!$search_word){
            $search_word = 0;
        }

        try {
            $result = DB::table('zipcodes')
                ->select('id', 'zipcode', 'city', 'state', 'country')
                ->where('zipcode', 'LIKE', $search_word . '%')
                ->orWhere('city', 'like', $search_word . '%')
                ->paginate($limit)
                ->toArray();
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }


        if(!empty($result)){
            return $result;
        }

        return false;
    }
}
