<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;


class MenuSection extends Model
{
    /**
     * Function to get list of all sections
     *
     * @return array
     */
    public function getAll(){
        return DB::table('menu_section')
            ->get()
            ->toArray();
    }

    /**
     * Function to get list of sections by Menu ID
     *
     * @param $menu_id
     * @return array
     */
    public function getSectionsByMenuId($menu_id){
        return DB::table('menu_section')
            ->where('menu_id', $menu_id)
            ->get()
            ->toArray();
    }

    /**
     * Function to get list of sections by Restaurant ID
     *
     * @param $restaurant_id
     * @return array
     */
    public function getSectionsByRestaurantId($restaurant_id){
        $result =  DB::table('menus as m')
            ->select('ms.*')
            ->leftJoin('menu_section as ms', 'ms.menu_id', '=', 'm.id')
            ->where('m.restaurant_id', $restaurant_id)
            ->where('ms.id', '!=', null)
            ->orderBy('ms.order')
            ->get()
            ->toArray();

        if($result){
            return ['count' => count($result), 'data' => $result];
        }

        return ['count' => 0, 'data' => []];
    }

    /**
     * Function to get Category By ID
     *
     * @param $id
     * @return mixed
     */
    public function getSectionById($id){
        if(!$id){
            return false;
        }

        $section_query = DB::table('menu_section')
            ->where('id', $id)
            ->get()
            ->first();

        $section_query->extra_lists = [];
        $section_query->extras = [];

        $extras_result = (new Extra())->getAllBySectionId($id);
        if(isset($extras_result['extra_lists'])) {
            $section_query->extra_lists = $extras_result['extra_lists'];
        }

        if(isset($extras_result['extras'])){
            $section_query->extras = $extras_result['extras'];
        }

        return $section_query;
    }

    /**
     * Function to save / update section
     *
     * @param $data
     * @return mixed
     */
    public function saveSection($id, $data){
        if(empty($data)){
            abort(422, 'Required data is empty');
        }

        if(isset($data['restaurant_id'])) {
            $menu_id = (new Menu())->getMenuByIdRestaurantId($data['restaurant_id']);
            $data['menu_id'] = $menu_id->id;
            unset($data['restaurant_id']);
        }

        if($id && $id != 0){
            try {
                // Update category
                DB::table('menu_section')
                    ->where('id', $id)
                    ->update($data);

                return $this->getSectionById($id);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else {
            try {
                $id = DB::table('menu_section')
                    ->insertGetId($data);

                return $this->getSectionById($id);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }
    }

    /**
     * Function to delete menu section (category)
     *
     * @param $id
     * @return bool|string
     */
    public function deleteSection($id){
        if(!$id){
            return false;
        }

        try {
            DB::table('menu_section')
                ->where('id', $id)
                ->delete();

            DB::table('menu_item')
                ->where('section_id', $id)
                ->delete();

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }
    }

    /**
     * Function to get categories and dishes by menu id
     *
     * @param $menu_id
     * @return array
     */
    public function getSectionsAndItemsById($menu_id){
        $query_result = DB::table('menu_section as ms')
            ->select('ms.name as section_name', 'mi.id as item_id', 'mi.name as item_name', 'mi.description', 'mi.picture as item_picture', 'mi.price as item_price')
            ->join('menu_item as mi', 'mi.section_id', '=', 'ms.id')
            ->where('ms.menu_id', $menu_id)
            ->get()
            ->toArray();

        $result = [];

        foreach ($query_result as $key => $item){
            $result[$item->section_name][$key]['item_id'] = $item->item_id;
            $result[$item->section_name][$key]['item_name'] = $item->item_name;
            $result[$item->section_name][$key]['item_description'] = $item->description;
            $result[$item->section_name][$key]['item_picture'] = $item->item_picture;
            $result[$item->section_name][$key]['item_price'] = $item->item_price;
        }

       return $result;
    }

    /**
     * Function to update categories ordering
     *
     * @param $data
     * @return bool
     */
    public function updateOrdering($data){
        if(empty($data)){
            return false;
        }

        try {
            ModelHelper::multipleUpdate('menu_section', $data, 'id');

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }
}
