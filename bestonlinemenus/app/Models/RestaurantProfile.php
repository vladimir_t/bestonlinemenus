<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use Mockery\Exception;
use Illuminate\Support\Facades\Mail;
use stdClass;

class RestaurantProfile extends Model
{

 /**
  * Model Fields
  * id - int
  * user_id - int
  * agent_name - varchar
  * subscription - enum('1 year', '2 years', 'monthly')
  * restaurant_name - varchar
  * manager_name - varchar
  * manager_email - varchar
  * delivery_email - varchar
  * cuisine_type - varchar
  * phone_number - varchar
  * phone_number2 - varchar
  * sales_tax - varchar
  * address - varchar
  * zipcode - varchar
  * tags - varchar
  * status - enum('active', 'blocked')
  * page_id - int
  * menu_id - int
  * created_at - timestamp
  * updated_at - timestamp
  * */

    /**
     * Function to gel list of profiles
     *
     * @return bool|array
     */
    public function getAllProfiles($request_data, $is_active = false){

        $where = [
            'u.role' => 'restaurant'
        ];

        if(isset($request_data->agent_name)){
            $where['sa.agent_name'] = $request_data->agent_name;
        }

        if(isset($request_data->status)){
            $where['rp.status'] = $request_data->status;
        }elseif($is_active){
            $where['rp.status'] = 'active';
        }

        if(isset($request_data->subscription)){
            $where['rp.subscription'] = $request_data->subscription;
        }

        if(isset($request_data->cuisine)){
            $where['rp.cuisine_type'] = $request_data->cuisine;
        }

        if(isset($request_data->deliveryMethod)){
            $where['rp.delivery_method'] = $request_data->deliveryMethod;
        }

        if(isset($request_data->search)){
            $where_search = ['rp.restaurant_name', 'LIKE',  '%' . $request_data->search . '%'];
            array_push($where, $where_search);
        }

        $sort_order = 'desc';
        if(isset($request_data->sort_order)){
            $sort_order = $request_data->sort_order;
        }

        $sort_field = 'restaurant_name';
        $sort_by_revenue = false;
        if(isset($request_data->sort_field)){
            if($request_data->sort_field == 'onboarded'){
                $sort_field = 'rp.created_at';
            }elseif($request_data->sort_field == 'revenue'){
                $sort_by_revenue = true;
            }else{
                $sort_field = $request_data->sort_field;
            }

        }

        $limit = 10;
        if(isset($request_data->limit)){
            $limit = $request_data->limit;
        }

        if(isset($request_data->onboardingPeriodStart)) {
            $where_search = ['rp.created_at', '>=', $request_data->onboardingPeriodStart . ' 00:00:00'];
            array_push($where, $where_search);
        }

        if(isset($request_data->onboardingPeriodEnd)){
            $where_search = ['rp.created_at', '<=',  $request_data->onboardingPeriodEnd . ' 23:59:59'];
            array_push($where, $where_search);
        }

        $result = DB::table('users as u')
            ->select('u.id', 'rp.*', 'sa.agent_name')
            ->leftJoin('restaurant_profile as rp', 'rp.user_id', '=', 'u.id')
            ->leftJoin('sales_agents_relation as sar', 'sar.restaurant_id', '=', 'rp.id')
            ->leftJoin('sales_agents as sa', 'sa.id', '=', 'sar.agent_id')
            ->where($where)
            ->orderBy($sort_field, $sort_order)
            ->paginate($limit)
            ->toArray();

        if(!empty($result)){
            foreach ($result['data'] as $key => $profile){
                //cast to integer
                $profile->sales_tax = (float) $profile->sales_tax;

                // get revenue
                $profile->revenue = (new Order())->getTotalRevenue($profile->id);

                // Check if each restaurant have working hours and if yes - add working hours to the main object
                $hours = (new WorkingHoursRelation())->getHoursByRestaurantId($profile->id);
                if($hours){
                    $profile->hours = $hours;
                }

                $result['data'][$key] = $profile;
            }

            if($sort_by_revenue){
                if($sort_order != 'desc') {
                    array_multisort(array_column($result['data'], 'revenue'), SORT_ASC);
                }else{
                    array_multisort(array_column($result['data'], 'revenue'), SORT_DESC);
                }
            }

            if(isset($request_data->revenue)) {
                $revenue_arr = [];
                foreach ($result['data'] as $key => $profile){
                    if($profile->revenue == $request_data->revenue){
                        array_push($revenue_arr, $result['data'][$key]);
                    }
                }
                $result['data'] = $revenue_arr;
            }

            return $result;
        }

        return false;
    }

    /**
     * Function to get Cuisine types array
     *
     * @return array|bool
     */
    public function getCousineTypes(){
        $result = DB::table('cuisine_type')
            ->get()->toArray();

        $map_array = [];

        if(!empty($result)){
            foreach ($result as $res){
                array_push($map_array, ['name' => $res->cuisine_name, 'value' => $res->id]);
            }
            if(!empty($map_array)){
                return $map_array;
            }
        }

        return false;
    }

    /**
     * Function to get restaurant profile by ID
     *
     * @param $profile_id
     * @return bool
     */
    public function getProfileById($profile_id, $isClient = false){
        $result = DB::table('users as u')
            ->select('u.id', 'rp.*', 'sa.agent_name', 'ct.cuisine_name', 'zc.zipcode as zip_code')
            ->leftJoin('restaurant_profile as rp', 'rp.user_id', '=', 'u.id')
            ->leftJoin('sales_agents_relation as sar', 'sar.restaurant_id', '=', 'rp.id')
            ->leftJoin('sales_agents as sa', 'sa.id', '=', 'sar.agent_id')
            ->leftJoin('cuisine_type as ct', 'ct.id', '=', 'rp.cuisine_type')
            ->leftJoin('zipcodes as zc', 'zc.id', '=', 'rp.zipcode')
            ->where('u.role', 'restaurant')
            ->where('rp.id', $profile_id)
            ->get()
            ->first();


        if(!empty($result)){
            //cast to integer
            $result->sales_tax = (float) $result->sales_tax;
            $result->revenue = (new Order())->getTotalRevenue($result->id);

            // Check if each restaurant have working hours and if yes - add working hours to the main object
            $hours = (new WorkingHoursRelation())->getHoursByRestaurantId($result->id);
            if($hours){
                $result->hours = $hours;
            }else{
                $result->hours = new stdClass();
            }

            if($isClient){
                $result->page_info = (new Pages())->getPageInfoClient($result->id);
            }

            // Check if we have Stripe connection for this profile
            $result->connected_stripe = false;
            if((new StripeUsers())->isConnected($profile_id)){
                $result->connected_stripe = true;
            }

            return $result;
        }

        return false;
    }

    /**
     * Function to get restaurant info by ID
     *
     * @param $restaurant_id
     * @return bool
     */
    public function getRestaurantByID($restaurant_id){
        $result = DB::table('restaurant_profile')
            ->where('id', $restaurant_id)
            ->get()
            ->first();


        if(!empty($result)){
            return $result;
        }

        return false;
    }


    public function getRestaurantAndMenuByID($restaurant_id){
        if(!$restaurant_id){
            return false;
        }

        $result = [];

        //Get Restaurant info
        $restaurant = DB::table('restaurant_profile as rp')
            ->select('rp.*', 'm.*')
            ->leftJoin('menus as m', 'm.id', '=', 'rp.menu_id')
            ->where('rp.id', $restaurant_id)
            ->get()
            ->first();

        if($restaurant){
            $result['restaurant'] = $restaurant;
        }

        //Get menu sections
        $sections = DB::table('menu_section')
            ->where('menu_id', $restaurant->menu_id)
            ->get()
            ->toArray();

        //Get menu items
        $items = DB::table('menu_item')
            ->where('menu_id', $restaurant->menu_id)
            ->get()
            ->toArray();

        if($sections){
            foreach ($sections as $key => $section){
                $result['sections'][$key]['id'] = $section->id;
                $result['sections'][$key]['name'] = $section->name;
                $result['sections'][$key]['description'] = $section->description;
                foreach ($items as $item){
                    if($item->section_id == $section->id){
                        $result['sections'][$key]['items'][] = $item;
                    }
                }
            }
        }

        if(!empty($result)){
            return $result;
        }

        return false;
    }

    /**
     * Function to save restaurant profile
     *
     * @param $id
     * @param $data
     * @return bool|string
     */
    public function saveProfile($id, $data, $returnList = false){
        if(empty($data)){
            return false;
        }

        $agent_name = '';
        if(isset($data['agent_name']) && $data['agent_name']){
            $agent_name = $data['agent_name'];
            unset($data['agent_name']);
        }elseif(isset($data['agent_name'])){
            if(is_null($data['agent_name']) || !$data['agent_name']){
                unset($data['agent_name']);
            }
        }

        if(isset($data['hours'])){
            $this->setWorkingHours($id, $data['hours']);
            unset($data['hours']);
        }

        // find cuisine type or create new one
        if(isset($data['cuisine_type'])){
            if(!is_numeric($data['cuisine_type'])){
                $cuisine_type = (new CuisineType())->getByName($data['cuisine_type']);
                if($cuisine_type && !empty($cuisine_type)){
                    $data['cuisine_type'] = $cuisine_type->id;
                }else{
                    $data['cuisine_type'] = DB::table('cuisine_type')
                        ->insertGetId(['cuisine_name' => $data['cuisine_type']]);
                }
            }
        }

        if($id != 0){
            try{
                DB::table('restaurant_profile')
                    ->where('id', $id)
                    ->update($data);

                if(!$agent_name){
                    DB::table('sales_agents_relation')
                        ->where('restaurant_id', $id)
                        ->delete();
                }

                if($agent_name) {
                    // Update sales agents
                    $this->saveSalesAgent($id, $agent_name);
                }

                if($returnList){
                    return $this->getAllProfiles([]);
                }else{
                    return $this->getProfileById($id);
                }
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else{
            try {
                // Check unique name
                $name = User::where('name', $data['restaurant_name'])->orWhere('email', $data['manager_email'])->get()->first();
                if($name){
                     abort(422, "Restaurant name or Manager email is not unique!");
                }

                try {
                    $user = User::create([
                        'name' => $data['restaurant_name'],
                        'email' => $data['manager_email'],
                        'password' => bcrypt($this->randomPassword()),
                        'role' => 'restaurant'
                    ]);

                    $data['user_id'] = $user->id;
                }catch (\Illuminate\Database\QueryException $exception){
                    Log::error($exception->getMessage(),  [
                        'code' => $exception->getCode(),
                    ]);
                }

                try {
                    $profile = DB::table('restaurant_profile')
                        ->insertGetId($data);
                }catch (\Illuminate\Database\QueryException $exception){
                    Log::error($exception->getMessage(),  [
                        'code' => $exception->getCode(),
                    ]);
                }

                try {
                    // Create default restaurant menu
                    DB::table('menus')
                        ->insert([
                            'restaurant_id' => $profile,
                            'name' => $data['restaurant_name']
                        ]);
                }catch (\Illuminate\Database\QueryException $exception){
                    Log::error($exception->getMessage(),  [
                        'code' => $exception->getCode(),
                    ]);
                }

                try {
                    // Create default page designer record
                    DB::table('pages')
                        ->insert([
                            'restaurant_id' => $profile,
                            'url' => preg_replace('/\s+/', '_', strtolower(trim($data['restaurant_name']))),
                            'menu_page_name' => $data['restaurant_name']
                        ]);

                    if ($agent_name) {
                        // Create Sales Agent record
                        $this->saveSalesAgent($profile, $agent_name);
                    }
                }catch (\Illuminate\Database\QueryException $exception){
                    Log::error($exception->getMessage(),  [
                        'code' => $exception->getCode(),
                    ]);
                }

                // Send mail to Restaurant manager
                $this->toAddress = $data['manager_email'];
                $mbody = array(
                    'name' => $data['restaurant_name'],
                    'email' => $data['manager_email'],
                    'reset_link' => env('FRONT_URL') . '/password/reset/',
                    'token' => app('auth.password.broker')->createToken($user)
                );

                Mail::send('emails.create', $mbody, function($message)
                {
                    $message
                        ->from('support@bestonlinemenus.com')
                        ->to($this->toAddress)
                        ->subject('Your restaurant created!');
                });

                if($returnList){
                    return $this->getAllProfiles([]);
                }else{
                    return $this->getProfileById($profile);
                }
            }catch (Exception $e){
                Log::error($e->getMessage(),  [
                    'code' => $e->getCode(),
                ]);
            }
        }
    }


    /**
     * Function to save relation between profile and sales agent
     *
     * @param $profile_id
     * @param $agent_name
     * @return bool|string
     */
    public function saveSalesAgent($profile_id, $agent_name){
        $agent = DB::table('sales_agents')
            ->select('id')
            ->where('agent_name', $agent_name)
            ->get()
            ->first();

        if(!$agent){
            try {
                $agent_id = DB::table('sales_agents')
                    ->insertGetId([
                        'agent_name' => $agent_name
                    ]);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else{
            $agent_id = $agent->id;
        }

        $relation_record = DB::table('sales_agents_relation')
            ->where('restaurant_id', $profile_id)
            ->get()
            ->first();

        if(!$relation_record){
            try {
                DB::table('sales_agents_relation')
                    ->insert([
                        'restaurant_id' => $profile_id,
                        'agent_id' => $agent_id
                    ]);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else{
            try {
                DB::table('sales_agents_relation')
                    ->where(['restaurant_id' => $profile_id])
                    ->update([
                        'agent_id' => $agent_id
                    ]);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }
        return true;
    }


    /**
     * Function to delete restaurant profile
     *
     * @param $id
     * @return bool|string
     */
    public function deleteProfile($id){
        if(!$id){
            return false;
        }

        try{
            $user_search = DB::table('restaurant_profile')
                ->select('user_id')
                ->where('id', $id)
                ->get()
                ->first();

            DB::table('users')
                ->where('id', $user_search->user_id)
                ->delete();

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }
    }

    /**
     * Function to get restaurants list
     *
     * @return bool
     */
    public function getRestaurantsList(){
        $result = DB::table('restaurant_profile')
            ->select('id', 'restaurant_name')
            ->get()
            ->toArray();


        if(!empty($result)){
            return $result;
        }

        return false;
    }

    /**
     * Function to get restaurant profile ID
     *
     * @param $user_id
     * @return bool
     */
    public function getProfileID($user_id){
        $result = DB::table('restaurant_profile')
            ->select('id')
            ->where('user_id', $user_id)
            ->get()
            ->first();

        if($result){
            return $result->id;
        }

        return false;
    }

    /**
     * Function to get static dictionary filters list
     */
    public function getDictionary(){
        $result_array = [];

        $cuisine = $this->getCousineTypes();

        if(!empty($cuisine)){
            $result_array['cuisine'] = $cuisine;
        }

        // Subscriptions
        $result_array['subscriptions'] = [
            ['name'=>'1 year', 'value' => '1 year'],
            ['name'=>'2 years', 'value' => '2 years'],
            ['name'=>'Monthly', 'value' => 'monthly']
        ];

        // Delivery method
        $result_array['delivery_method'] = [
            ['name' => 'Pickup Only', 'value' => 'Pickup Only'],
            ['name' => 'Pickup&Delivery', 'value' => 'Pickup&Delivery'],
            ['name' => 'Delivery Only', 'value' => 'Delivery Only']
        ];

        // Status
        $result_array['status'] = ['active', 'blocked'];

        // Revenue values
        $result_array['revenues'] = (new Order())->getAllRevenues();

        if(!empty($result_array)){
            return $result_array;
        }

        return false;
    }


    /**
     * Function to save working hours
     *
     * @param $restaurant_id
     * @param $hours
     * @return bool
     */
    public function setWorkingHours($restaurant_id, $hours){
        if(!$hours){
            return false;
        }

        $working_hours_array = [];
        foreach ($hours as $type => $value){
            foreach ($value as $day => $val){
                array_push($working_hours_array, [
                   'restaurant_id' => $restaurant_id,
                    'type' => $type,
                    'day' => $day,
                    'start_date' => (isset($val['start'])) ? $val['start'] : '',
                    'end_date' => (isset($val['end'])) ? $val['end'] : '',
                    'isClosed' => (isset($val['isClosed']) && $val['isClosed']) ? "true" : "false"
                ]);
            }
        }

        if(!empty($working_hours_array)){
            if((new WorkingHoursRelation())->insertOrUpdateHours($working_hours_array)){
                return true;
            }
        }

        return false;
    }


    /**
     * Function to generate random password string
     *
     * @return string
     */
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); //turn the array into a string
    }
}
