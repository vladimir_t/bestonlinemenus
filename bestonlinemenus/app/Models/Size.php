<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Size extends Model
{
    protected $_table = 'size';

    /**
     * Insert or update rows
     *
     * @param array $rows
     * @return mixed
     */
    function insertOrUpdateSize(array $rows){
        return ModelHelper::insertOrUpdate($this->_table, $rows);
    }

    /**
     * Function get size items by menu item ID
     *
     * @param $item_id
     * @return mixed
     */
    function getAllByItemId($item_id){
        if(!$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('id', 'name', 'price')
            ->where('item_id', $item_id)
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

    }

    /**
     * Function get size items by menu item ID
     *
     * @param $section_id
     * @return mixed
     */
    function getAllBySectionId($section_id){
        if(!$section_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('id', 'name', 'price')
            ->where('section_id', $section_id)
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

    }

    /**
     * Function to get list of id's of size items
     *
     * @param $item_id
     * @return mixed
     */
    function getIdsList($item_id){
        if(!$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('id')
            ->where('item_id', $item_id)
            ->get()
            ->toArray();
    }

    /**
     * Function to delete item size record by id
     *
     * @param array $id_array
     * @return mixed
     *
     */
    function deleteItemsSize($id_array){
        if(empty($id_array)){
            return false;
        }

        return DB::table($this->_table)
            ->whereIn('id', $id_array)
            ->delete();
    }


    /**
     * Function to save item sizes.
     *
     * @param $data
     * @param $item_id
     * @return mixed
     */
    function saveSizeData($data, $item_id){
        if(!$item_id){
            return false;
        }

        $response = [];
        $response['item_id'] = $item_id;

        if(empty($data['items'])){
            $this->deleteItemsByItemID($item_id);
            // Update withSize status
            (new MenuItem())->saveItem($item_id, ['withSizes' => 'false']);

            //Get updated item info
            $response['sizes'] = $this->getAllByItemId($item_id);

            return $response;
        }

        $section_id = 0;
        if(isset($data['category_id'])){
            $section_id = $data['category_id'];
            unset($data['category_id']);
        }

        // Get current size rows
        $current_size_rows = $this->getIdsList($item_id);

        // Get deleted rows id's
        $deleted_size_rows = ModelHelper::getDeletedIDs($current_size_rows, $data['items']);
        if(!empty($deleted_size_rows)) {
            // Remove deleted rows
            $this->deleteItemsSize($deleted_size_rows);
        }

        $size_rows = ModelHelper::generateExtraAndSizeArray($section_id, $data['items'], $item_id);
        if(!empty($size_rows)) {
            $update_rows = [];
            $insert_rows = [];
            foreach ($size_rows as $row){
                if(isset($row['id']) && $row['id'] != 0){
                    array_push($update_rows, $row);
                }else{
                    array_push($insert_rows, $row);
                }
            }

            if($insert_rows) {
                // Update multiple rows
                if ($this->insertOrUpdateSize($insert_rows)) {
                    // Update withSize status
                    (new MenuItem())->saveItem($item_id, ['withSizes' => 'true']);
                }
            }

            if($update_rows){
                ModelHelper::multipleUpdate($this->_table, $update_rows, 'id');
            }

            //Get updated item info
            $response['sizes'] = $this->getAllByItemId($item_id);

            if(!empty($response)){
                return $response;
            }
        }

        return false;
    }

    /**
     * Function to delete rows by item ID
     *
     * @param $item_id
     * @return bool|int
     */
    public function deleteItemsByItemID($item_id){
        if(!$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->where('item_id', $item_id)
            ->delete();
    }

}
