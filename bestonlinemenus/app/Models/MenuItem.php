<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MenuItem extends Model
{
    /**
     * Function to get all menu items
     *
     * @return mixed
     */
    public function getAll(){
        return DB::table('menu_item')
            ->get()
            ->toArray();
    }

    /**
     * Function to get menu item by category and name
     *
     * @param $category_id
     * @param bool $name
     * @param int $limit
     * @return array|bool
     */
    public function getItems($category_id, $name = false, $limit = 10){
        if(!$category_id){
            return false;
        }

        $where = [
            'mi.section_id' => $category_id
        ];

        if($name){
            $where_search = ['mi.name', 'LIKE',  '%' . $name . '%'];
            array_push($where, $where_search);
        }

        $result = DB::table('menu_item as mi')
            ->where($where)
            ->orderBy('mi.order')
            ->paginate($limit)
            ->toArray();

        foreach ($result['data'] as $key => $val){
            // change isPublished to bool
            if(isset($val->isPublished)){
                $result['data'][$key]->isPublished = ($val->isPublished == "true") ? true : false;
            }

            // Get Extra
            $extra_result = (new Extra())->getAllByItemId($val->id);
            if(!empty($extra_result)){
                if(isset($extra_result['extra_lists'])){
                    $result['data'][$key]->extra_lists = $extra_result['extra_lists'];
                }
                if(isset($extra_result['extras'])){
                    $result['data'][$key]->extras = $extra_result['extras'];
                }
            }else{
                $result['data'][$key]->extras = [];
                $result['data'][$key]->extra_lists = [];
            }

            // Get Sizes array
            $size_result = (new Size())->getAllByItemId($val->id);
            if(!empty($size_result)){
                $result['data'][$key]->sizes = $size_result;
            }else{
                $result['data'][$key]->sizes = [];
            }
        }

        if($result){
            return $result;
        }

        return ['total' => 0, 'data' => []];
    }


    /**
     * Get dish list by category name and restaurant url
     *
     * @param $category_name
     * @param $restarant_url
     * @param bool $name
     * @param int $limit
     * @param bool $withSizes
     * @param bool $withExtras
     * @return array|bool
     */
    public function getItemsByCatName($category_name, $restarant_url, $name = false, $limit = 10, $withSizes = false, $withExtras = false){
        if(!$category_name && !$restarant_url){
            return false;
        }

        $categories = DB::table('pages as p')
            ->select('ms.id', 'ms.name')
            ->leftJoin('menus as m', 'm.restaurant_id', '=', 'p.restaurant_id')
            ->leftJoin('menu_section as ms', 'ms.menu_id', '=', 'm.id')
            ->where('p.url', $restarant_url)
            ->get()
            ->toArray();

        $category_id = 0;
        $result = [];

        foreach ($categories as $category){
            if($category->name == $category_name){
                $category_id = $category->id;
            }
        }

        if($category_id){
            $result = $this->getItems($category_id, $name, $limit, $withSizes, $withExtras);
        }

        if(!empty($result)){
            return $result;
        }

        return ['total' => 0, 'data' => []];
    }


    /**
     * Function to get menu item by ID
     *
     * @param $item_id
     * @return mixed
     */
    public function getItemById($item_id){
        $result = DB::table('menu_item')
            ->where('id', $item_id)
            ->get()
            ->first();

        $result->isPublished = ($result->isPublished == "true") ? true : false;

        // Get Sizes array
        $size_result = (new Size())->getAllByItemId($item_id);
        if(!empty($size_result)){
            $result->sizes = $size_result;
        }

        // Get Extra array
        $extra_result = (new Extra())->getAllByItemId($item_id);

        $result->extra_lists = [];
        $result->extras = [];
        if(!empty($extra_result)){
            if(isset($extra_result['extra_lists'])){
                $result->extra_lists = $extra_result['extra_lists'];
            }

            if(isset($extra_result['extras'])){
                $result->extras = $extra_result['extras'];
            }
        }


        if($result){
            return $result;
        }

        return false;
    }

    /**
     * Function to save menu item
     *
     * @param $id
     * @param $data
     * @param string $pic_path
     * @return mixed
     */
    public function saveItem($id, $data, $pic_path = '')
    {
        if(isset($data['category_id'])) {
            $data['section_id'] = $data['category_id'];
            unset($data['category_id']);
        }

        if(isset($data['id'])) {
            unset($data['id']);
        }

        if($pic_path){
            $data['picture'] = $pic_path;
        }

        if(!$id || $id == "undefined" || $id == 0) {
            //Insert
            try {
                $id = DB::table('menu_item')
                    ->insertGetId($data);

                return $this->getItemById($id);
            } catch (\Illuminate\Database\QueryException $e) {
                Log::error($e->getMessage(),  [
                    'code' => $e->getCode(),
                ]);
            }
        }else{
            //Update
            try {
                DB::table('menu_item')
                    ->where('id', $id)
                    ->update($data);

                return $this->getItemById($id);
            } catch (\Illuminate\Database\QueryException $e) {
                Log::error($e->getMessage(),  [
                    'code' => $e->getCode(),
                ]);
            }
        }
    }

    /**
     * Function to delete menu item
     *
     * @param $item_id
     * @return mixed
     */
    public function deleteItem($item_id){
        if(!$item_id){
            return false;
        }

        try{
            DB::table('menu_item')
                ->where('id', $item_id)
                ->delete();

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }
    }

    /**
     * Function to publish menu items (dish)
     *
     * @param $items_array
     * @return bool|string
     */
    public function publishItems($items_array, $status = "false"){
        if(empty($items_array)){
            return false;
        }

        try{
            DB::table('menu_item')
                ->whereIn('id', $items_array)
                ->update([
                    'date_published' => date('Y-m-d H:i:s', time()),
                    'isPublished' => $status
                ]);

            return true;

        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }
    }

    /**
     * Function to change ordering position
     *
     * @param $data
     * @return bool
     */
    public function updateOrdering($data){
        if(empty($data)){
            return false;
        }

        try {
            ModelHelper::multipleUpdate('menu_item', $data, 'id');

            return true;
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }

    /**
     * Function to get category id by item id
     *
     * @param $item_id
     * @return bool|mixed
     */
    public function getCatID($item_id){
        if(!$item_id){
            return false;
        }

        try {
            $result = DB::table('menu_item')
                ->select('section_id')
                ->where('id', $item_id)
                ->first();

            if($result){
                return $result->section_id;
            }
        }catch (\Illuminate\Database\QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }
}
