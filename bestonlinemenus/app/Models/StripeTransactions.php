<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class StripeTransactions extends Model
{
    public function saveAction($restaurant_id, $stripe_partner_id, $transaction_id){
        if(!$restaurant_id || !$stripe_partner_id || !$transaction_id){
            return false;
        }

        try{
            $id = DB::table('transactions')
                ->insertGetId([
                    'restaurant_id' => $restaurant_id,
                    'stripe_user_id' => $stripe_partner_id,
                    'stripe_transaction_id' => $transaction_id,
                    'created_at' => date('Y-m-d h:i:s a', time())
                ]);

            return $id;
        }catch (QueryException $exception){
            Log::error($exception->getMessage(),  [
                'code' => $exception->getCode(),
            ]);
        }

        return false;
    }

    /**
     *
     * @param $transaction_id
     * @return bool
     */
    public function getChargeID($transaction_id){
        if(!$transaction_id){
            return false;
        }

        $result = DB::table('transactions')
            ->select('stripe_transaction_id')
            ->where('id', $transaction_id)
            ->first();

        if($result) {
            return $result->stripe_transaction_id;
        }

        return false;
    }
}
