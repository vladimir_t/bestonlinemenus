<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExtraItemRelation extends Model
{
    protected $_table = 'extra_item_relation';

    /**
     * Function to save relations
     *
     * @param $relations
     * @return mixed
     */
    public function saveRelations($relations){
       return ModelHelper::insertOrUpdate($this->_table, $relations);
    }

    /**
     * Function to delete relations
     *
     * @param $extra_ids
     * @param $item_id
     * @return bool|int
     */
    public function deleteRelations($extra_ids, $item_id){
        if(empty($extra_ids) && !$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->whereIn('extra_id', $extra_ids)
            ->where('item_id', $item_id)
            ->delete();
    }

    /**
     * Function to get extra ids by item id
     *
     * @param $item_id
     * @return array|bool
     */
    public function getExtrasByItemID($item_id){
        if(!$item_id){
            return false;
        }

        return DB::table($this->_table)
            ->select('extra_id')
            ->where('item_id', $item_id)
            ->get()
            ->toArray();
    }
}
