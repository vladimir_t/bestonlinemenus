<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderProductsExtra extends Model
{
    private $_table = 'order_products_extra';

    /**
     * Save order extra function
     *
     * @param $product_id
     * @param $data
     * @return bool|mixed
     */
    public function saveProductExtra($product_id, $data){
        if(!$product_id || empty($data)){
            return false;
        }

        $row = [];
        foreach ($data as $extra){
            $row['product_id'] = $product_id;
            $row['extra_id'] = $extra['id'];
            $row['amount'] = 1;

            if(isset($extra['amount'])){
                $row['amount'] = $extra['amount'];
            }

            try {
                DB::table($this->_table)
                    ->insert($row);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }
    }

    /**
     * Function to get extra info by product ID
     *
     * @param $product_id
     * @return array
     */
    public function getExtrasByProductID($product_id){
        if(!$product_id){
            return [];
        }

        return DB::table('order_products_extra as ope')
            ->select('e.id', 'e.name', 'e.price', 'ope.amount')
            ->leftJoin('extra as e', 'e.id', '=', 'ope.extra_id')
            ->where('ope.product_id', $product_id)
            ->get()
            ->toArray();
    }
}
