<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Pages extends Model
{
    /**
     * Get page info
     *
     * @param $page_id
     * @return mixed
     */
    public function getPageInfo($page_id){
        return DB::table('pages')
            ->where('id', $page_id)
            ->get()
            ->first();
    }

    /**
     * Get page info by restaurant ID
     *
     * @param $user_id
     * @return mixed
     */
    public function getPageInfoByProfileID($profile_id){
        return DB::table('pages as p')
            ->select('p.*', 'rp.restaurant_name')
            ->leftJoin('restaurant_profile as rp', 'rp.id', '=', 'p.restaurant_id')
            ->where('p.restaurant_id', $profile_id)
            ->get()
            ->first();
    }


    /**
     * Get page info for client by restaurant ID
     *
     * @param $user_id
     * @return mixed
     */
    public function getPageInfoClient($profile_id){
        return DB::table('pages')
            ->select('url', 'menu_page_name', 'nav_type', 'menu_navigation_color', 'menu_background_color', 'logo', 'background_picture')
            ->where('restaurant_id', $profile_id)
            ->get()
            ->first();
    }

    /**
     * Function to save page info
     *
     * @param $user_id
     * @param $data
     * @param string $logo_path
     * @param string $background_image
     * @return bool|string
     */
    public function savePage($data, $logo_path = '', $background_image = ''){
        if(empty($data)){
            return false;
        }

        // url replace
        $data['url'] = preg_replace('/\s+/', '_', trim($data['url']));

        if($logo_path){
            $data['logo'] = $logo_path;
        }

        if($background_image){
            $data['background_picture'] = $background_image;
        }else{
            unset($data['background_picture']);
        }

        if(isset($data['restaurant_id']) && $data['restaurant_id'] != 0){
            $restaurant_id = $data['restaurant_id'];
            // remove id fromdata array
            unset($data['restaurant_id']);

            try {
                DB::table('pages')
                    ->where('restaurant_id', $restaurant_id)
                    ->update($data);

                return $this->getPageInfoByProfileID($restaurant_id);
            }catch (\Illuminate\Database\QueryException $exception){
                Log::error($exception->getMessage(),  [
                    'code' => $exception->getCode(),
                ]);
            }
        }else{
            abort(422, 'Page designer ID required!');
        }
    }

    /**
     * Function to get Profile by URL
     *
     * @param $url_name
     * @return bool|mixed
     */
    public  function getProfileByUrl($url_name){
        if(!$url_name){
            return false;
        }

        $result = DB::table('pages as p')
            ->select('u.id', 'rp.*', 'sa.agent_name')
            ->leftJoin('restaurant_profile as rp', 'rp.id', '=', 'p.restaurant_id')
            ->leftJoin('users as u', 'u.id', '=', 'rp.user_id')
            ->leftJoin('sales_agents_relation as sar', 'sar.restaurant_id', '=', 'rp.id')
            ->leftJoin('sales_agents as sa', 'sa.id', '=', 'sar.agent_id')
            ->where('u.role', 'restaurant')
            ->where('p.url', $url_name)
            ->get()
            ->first();

        if(!empty($result)){
            //cast to integer
            $result->sales_tax = (float) $result->sales_tax;

            // Check if each restaurant have working hours and if yes - add working hours to the main object
            $hours = (new WorkingHoursRelation())->getHoursByRestaurantId($result->id);
            if($hours){
                $result->hours = $hours;
            }else{
                $result->hours = [];
            }

            $result->page_info = $this->getPageInfoClient($result->id);

            // Check if we have Stripe connection for this profile
            $result->connected_stripe = false;
            if((new StripeUsers())->isConnected($result->id)){
                $result->connected_stripe = true;
            }

            if($result->city){
                $result->city = ucfirst(strtolower($result->city));
            }

            return $result;
        }

        return false;
    }

    /**
     * Function to get categories by restaurant URL
     *
     * @param $restaurant_url
     * @return array
     */
    public function getSectionsByRestaurantUrl($restaurant_url){
        if(!$restaurant_url){
            return ['count' => 0, 'data' => []];
        }

        $result =  DB::table('pages as p')
            ->select('ms.*')
            ->leftJoin('menus as m', 'm.restaurant_id', '=', 'p.restaurant_id')
            ->leftJoin('menu_section as ms', 'ms.menu_id', '=', 'm.id')
            ->where('p.url', $restaurant_url)
            ->where('ms.id', '!=', null)
            ->where(DB::raw('(SELECT COUNT(`mi`.`id`) FROM `menu_item` as `mi` WHERE `mi`.`section_id` = `ms`.`id`)'), '>', 0)
            ->orderBy('ms.order')
            ->get()
            ->toArray();

        if($result){
            return ['count' => count($result), 'data' => $result];
        }

        return ['count' => 0, 'data' => []];
    }
}
