<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SalesAgents extends Model
{
    /**
     * Model Fields
     * id - int
     * restaurant_id - int
     * agent_name - varchar
     */


    /**
     * Function to get agents list
     *
     * @return array
     */
    public function getAllAgents($search_name = false, $limit = 10){
        $where = [];
        if($search_name){
            $where_search = ['agent_name', 'LIKE',  '%' . $search_name . '%'];
            array_push($where, $where_search);
        }

        $result = DB::table('sales_agents')
            ->select('id', 'agent_name')
            ->orderBy('agent_name', 'asc')
            ->where($where)
            ->paginate($limit)
            ->toArray();

        $result_array = [];
        if(!empty($result['data'])){
            foreach ($result['data'] as $agent){
                array_push($result_array, ['name' => $agent->agent_name, 'value' => $agent->id]);
            }
        }

        return ['count' => count($result_array), 'data' => $result_array];
    }
}
