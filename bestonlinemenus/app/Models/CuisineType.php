<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CuisineType extends Model
{
    /**
     * Function to return list with all cuisine types
     *
     * @return array|bool
     */
    public function getAll(){
        $result = DB::table('cuisine_type')
            ->get()
            ->toArray();

        if(!empty($result)){
            return $result;
        }

        return false;
    }

    /**
     * Function to return list of cuisine types by filter
     *
     * @param bool $name
     * @param int $limit
     * @return array
     */
    public function getByFilter($name = false, $limit = 10){
        $where = [];
        if($name){
            $where_search = ['cuisine_name', 'LIKE',  $name . '%'];
            array_push($where, $where_search);
        }

        $result = DB::table('cuisine_type')
            ->select('id', 'cuisine_name')
            ->orderBy('cuisine_name', 'asc')
            ->where($where)
            ->paginate($limit)
            ->toArray();

        $result_array = [];
        if(!empty($result['data'])){
            foreach ($result['data'] as $cuisine){
                array_push($result_array, ['name' => $cuisine->cuisine_name, 'value' => $cuisine->id]);
            }
        }

        return ['count' => $result['total'], 'data' => $result_array];
    }

    /**
     * Function to get cuisine type by name
     *
     * @param $name
     * @return array|bool
     */
    public function getByName($name){
        if(!$name){
            return false;
        }

        return DB::table('cuisine_type')
            ->where('cuisine_name', $name)
            ->first();
    }
}
