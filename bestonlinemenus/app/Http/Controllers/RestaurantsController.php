<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use Illuminate\Http\Request;
use App\Models\RestaurantProfile;

class RestaurantsController extends Controller
{
    public function index(Request $request){

        //Get all restaurants from DB
        $restaurants_list = ( new RestaurantProfile() )->getAllProfiles($request,true);

        return view('restaurants', ['restaurants' => $restaurants_list]);
    }


    public function getRestaurant($id){

        //Get restaurant info
        $restaurant_info = ( new RestaurantProfile() )->getRestaurantAndMenuByID($id);

        $page_info = (new Pages())->getPageInfo($restaurant_info['restaurant']->page_id);

        return view('restaurant', ['restaurant' => $restaurant_info, 'page_info' => $page_info]);
    }
}
