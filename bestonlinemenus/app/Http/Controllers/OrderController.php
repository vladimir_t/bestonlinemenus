<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderProducts;
use App\Models\RestaurantProfile;
use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Save order info action
     *
     * @param Request $request
     * @return string
     *
     * @SWG\Post(
     *   path="/api/order",
     *   summary="Save order info action",
     *   operationId="saveOrderAction",
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="formData",
     *     description="Restaurant id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Client name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="Client phone",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="Client city",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="street_address",
     *     in="formData",
     *     description="Client address",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="building",
     *     in="formData",
     *     description="Client building",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="appartment",
     *     in="formData",
     *     description="Client appartment",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="Client zipcode",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="payment_method",
     *     in="formData",
     *     description="Payment method",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="order",
     *     in="formData",
     *     description="Order info (JSON selected products array)",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="formData",
     *     description="Order status",
     *     required=false,
     *     enum={"new", "pending", "in_process", "complete", "refund"},
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="comment",
     *     in="formData",
     *     description="Client comment",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     *
     * @SWG\Patch(
     *   path="/api/order",
     *   summary="Update order info action",
     *   operationId="updateOrderAction",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Order id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Client name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="Client phone",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="Client city",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="street_address",
     *     in="formData",
     *     description="Client address",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="building",
     *     in="formData",
     *     description="Client building",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="appartment",
     *     in="formData",
     *     description="Client appartment",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="Client zipcode",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="formData",
     *     description="Order status",
     *     required=false,
     *     enum={"new", "pending", "in_process", "complete", "refund"},
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     */
    public function saveOrderAction(Request $request){
        $data = $request->all();

        if($request->method() == 'POST') {
            $validate_fields = [
                'restaurant_id' => 'required',
                'name' => 'required|max:255',
                'phone' => 'required|max:20'
            ];

            $validator = Validator::make($data, $validate_fields);


            if ($validator->fails()) {
                if (!is_null($validator->errors())) {
                    $messge = '';
                    foreach ($validator->errors()->all() as $msg) {
                        $messge .= $msg . ' ';
                    }
                } else {
                    $messge = "Please fill all required fields!";
                }

                return response()->json(['status' => 'error', 'message' => trim($messge), 'code' => 422], 422);
            }

            $save_status = (new Order())->saveOrderInfo($data);
            if($save_status){
                $this->sendSuccessOrderEmails($data['restaurant_id'], $data['email'], $save_status['order_id']);
                if($save_status){
                    return response()->json($save_status, 200);
                }
            }

        }elseif($request->method() == 'PATCH'){
            $updated_order = (new Order())->updateOrderClientInfo($request->id, $data);

            if($updated_order){
                return response()->json($updated_order, 200,[], JSON_NUMERIC_CHECK);
            }
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Function to update order products
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Patch(
     *   path="/api/product",
     *   summary="Update order product action",
     *   operationId="updateOrderProductsAction",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="order_id",
     *     in="formData",
     *     description="Order id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="products",
     *     in="formData",
     *     description="Order products info (JSON selected products array)",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="total_price",
     *     in="formData",
     *     description="Order total price",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     */
    public function updateOrderProductsAction(Request $request){
        if(!isset($request->order_id)){
            return response()->json(['status' => 'error', 'message' => 'Order id is required!', 'code' => 422], 422);
        }

        if(!isset($request->products)){
            return response()->json(['status' => 'error', 'message' => 'Products array is empty!', 'code' => 422], 422);
        }

        $updated_order = (new OrderProducts())->updateProducts($request->order_id, $request->products, $request->total_price);
        if($updated_order){
            return response()->json($updated_order, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\GET(
     *   path="/api/order",
     *   summary="Get orders info action",
     *   operationId="getOrdersList",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="restaurant_id",
     *     in="query",
     *     description="Restaurant id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="query",
     *     description="Order status",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="date",
     *     in="query",
     *     description="Filter by create date",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="revenue",
     *     in="query",
     *     description="Filter by revenue",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="start_date",
     *     in="query",
     *     description="Filter by date_updated period (start date)",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="stop_date",
     *     in="query",
     *     description="Filter by date_updated period (end date)",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit",
     *     required=false,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     */
    public function getOrdersList(Request $request){
        if(!isset($request->restaurant_id)){
            return response()->json(['status' => 'error', 'message' => 'Restaurant id is required!', 'code' => 422], 422);
        }

        $orders_list = (new Order())->getOrders($request->all());

        if(!empty($orders_list)){
            return  response()->json(['count' => $orders_list['total'], 'data' => $orders_list['data'], 'code' => 200], 200,[],JSON_NUMERIC_CHECK);
        }

        return response()->json(['count' => 0, 'data' => [], 'code' => 200], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\GET(
     *   path="/api/order/{id}",
     *   summary="Get order info action",
     *   operationId="getOrderById",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     description="Order id",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="Order not found!")
     * )
     */
    public function getOrderById(Request $request){
        if(!$request->id){
            return response()->json(['status' => 'error', 'message' => 'Order id is required!', 'code' => 422], 422);
        }

        $order_info = (new Order())->getOrderById($request->id);

        if(!empty($order_info)){
            return response()->json($order_info, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Order not found!', 'code' => 404], 404);

    }

    /**
     * Function to send emails to client and manager
     *
     *
     * @param $restaurant_id
     * @param $client_email
     * @param $order_id
     * @return bool
     */
    public function sendSuccessOrderEmails($restaurant_id, $client_email, $order_id){
        if(!$restaurant_id || !$order_id){
            return false;
        }

        // Get restaurant info
        $restaurant_profile = (new RestaurantProfile())->getRestaurantByID($restaurant_id);

        // Get order info
        $order_info = (new Order())->getOrderById($order_id);

        // Send mail to Restaurant manager
        try {
            $this->toAddress = $restaurant_profile->manager_email;
            $mbody = array(
                'name' => $restaurant_profile->manager_name,
                'restaurant_name' => $restaurant_profile->restaurant_name,
            );


            Mail::send('emails.order_manager', $mbody, function ($message) {
                $message
                    ->from('support@bestonlinemenus.com')
                    ->to($this->toAddress)
                    ->subject('Your restaurant have new order!');
            });
        }catch (Exception $e){
            Log::error($e->getMessage(),  [
                'code' => $e->getCode(),
            ]);
        }


        // Send mail to client
        try {
            $this->toAddressClient = $client_email;
            $mbody = array(
                'order_id' => $order_id,
                'client_name' => $order_info['name'],
                'restaurant_name' => $restaurant_profile->restaurant_name,
                'products' => $order_info['products'],
                'total_price' => $order_info['total_price'],
                'payment_method' => $order_info['payment_method']
            );

            Mail::send('emails.order_client', $mbody, function ($message) {
                $message
                    ->from('support@bestonlinemenus.com')
                    ->to($this->toAddressClient)
                    ->subject('Your order placed successfully!');
            });
        }catch (Exception $e){
            Log::error($e->getMessage(),  [
                'code' => $e->getCode(),
            ]);
        }

        return false;
    }

    /**
     * Function to get total revenue and total orders info by restaurant ID
     *
     * @param $restaurant_id
     * @return false|\Illuminate\Http\JsonResponse|string
     *
     * @SWG\GET(
     *   path="/api/order/total/{restaurant_id}",
     *   summary="Get total revenue and total orders completed info",
     *   operationId="getOrdersTotal",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong!")
     * )
     */
    public function getOrdersTotal($restaurant_id){
        if(!$restaurant_id){
            return response()->json(['status' => 'error', 'message' => 'Restaurant id is required!', 'code' => 422], 422);
        }

        // Get total revenue
        $total_revenue = (new Order())->getTotalRevenue($restaurant_id);

        // Get total completed orders
        $total_orders = (new Order())->getOrders(['restaurant_id' => $restaurant_id], true);

        if($total_revenue && $total_orders){
            return response()->json(['total_revenue' => $total_revenue, 'total_orders' => $total_orders['total'], 'code' => 200], 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to delete order by ID
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\DELETE(
     *   path="/api/order",
     *   summary="Delete order by ID",
     *   operationId="deleteOrder",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Order id",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong!")
     * )
     *
     */
    public function deleteOrder(Request $request){
        if(!$request->id){
            return response()->json(['status' => 'error', 'message' => 'Order id is required!', 'code' => 422], 422);
        }

        if((new Order())->deleteOrder($request->id)){
            return response()->json(['status' => 'success', 'message' => 'Your order deleted successfully!', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }
}
