<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Pages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminPageDesignerController extends Controller
{

    /**
     * Function to update info for page designer
     *
     * @param Request $request
     * @return false|\Illuminate\Http\JsonResponse|string
     *
     * @SWG\Post(
     *   path="/api/designer",
     *   summary="Update page designer action",
     *   operationId="savePageAction",
     *   consumes={"multipart/form-data"},
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="formData",
     *     description="Restaurant id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="url",
     *     in="formData",
     *     description="URL",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_page_name",
     *     in="formData",
     *     description="Page name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_navigation_color",
     *     in="formData",
     *     description="Navigation color",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_background_color",
     *     in="formData",
     *     description="Background color",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="logo",
     *     in="formData",
     *     description="Logo img",
     *     required=false,
     *     type="file"
     *   ),
     *   @SWG\Parameter(
     *     name="background_picture",
     *     in="formData",
     *     description="Background picture",
     *     required=false,
     *     type="file"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="missing required field"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     * @SWG\Patch(
     *   path="/api/designer",
     *   summary="Update page designer action",
     *   operationId="updatePageAction",
     *   consumes={"multipart/form-data"},
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="url",
     *     in="formData",
     *     description="URL",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_page_name",
     *     in="formData",
     *     description="Page name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_navigation_color",
     *     in="formData",
     *     description="Navigation color",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_background_color",
     *     in="formData",
     *     description="Background color",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="logo",
     *     in="formData",
     *     description="Logo img",
     *     required=false,
     *     type="file"
     *   ),
     *   @SWG\Parameter(
     *     name="background_picture",
     *     in="formData",
     *     description="Background picture",
     *     required=false,
     *     type="file"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="missing required field"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function savePageAction(Request $request){

        // Validate request data
        $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required',
            'url' => 'required|max:255',
        ]);

        if($validator->fails()){
            if(!is_null($validator->errors())){
                $messge = '';
                foreach ($validator->errors()->all() as $msg){
                    $messge .= $msg . ' ';
                }
            }else{
                $messge = "Please fill all required fields!";
            }

            return response()->json(['status' => 'error', 'message' => trim($messge), 'code' => 422], 422);
        }

        $logo_path = '';
        $back_path = '';

        if( $request->hasFile('logo') ) {
            $img = file_get_contents($request->file('logo'));
            $imagetype = getimagesizefromstring($img);
            $image_ext = $this->get_extension($imagetype['mime']);
            $filename = "logo_" . time() . $image_ext;

            $root = Storage::url('public/page/logo/' . $request->restaurant_id . '/');

            $exists = Storage::disk('local')->exists($root . $filename);
            if ($exists) {
                Storage::delete($root . $filename);
            }

            Storage::putFileAs($root, $request->file('logo'), $filename);

            $logo_path = env('APP_URL') . $root . $filename;
        }

        if( $request->hasFile('background_picture') ) {
            $back_root = Storage::url('public/page/background/' . $request->restaurant_id . '/');

            $img_back = file_get_contents($request->file('background_picture'));
            $imagetype_back = getimagesizefromstring($img_back);
            $image_ext_back = $this->get_extension($imagetype_back['mime']);
            $back_filename = "background_" . time() . $image_ext_back;

            $back_exists = Storage::disk('local')->exists($back_root . $back_filename);
            if ($back_exists) {
                Storage::delete($back_root . $back_filename);
            }

            Storage::putFileAs($back_root, $request->file('background_picture'), $back_filename);

            $back_path = env('APP_URL') . $back_root . $back_filename;
        }

        $save_page_result = (new Pages())->savePage($request->all(), $logo_path, $back_path);
        if($save_page_result){
            return response()->json($save_page_result, 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to get page info by restaurant ID
     *
     * @param Request $request
     * @return false|\Illuminate\Http\JsonResponse|string
     *
     * @SWG\Get(
     *   path="/api/designer/{restaurant_id}",
     *   summary="Get page info",
     *   operationId="getPageInfo",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="something is wrong, please contact support"),
     *   @SWG\Response(response=422, description="missing required field")
     * )
     *
     */
    public function getPageInfo(Request $request){
        if(!$request->restaurant_id){
            return response()->json(['status' => 'error', 'message' => 'Page ID is required!', 'code' => 422], 422);
        }

        $page_info = (new Pages())->getPageInfoByProfileID($request->restaurant_id);
        if($page_info){
            return response()->json($page_info, 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Small hack function to get file extension from the blob string
     *
     * @param $imagetype
     * @return bool|string
     */
    function get_extension($imagetype)
    {
        if(empty($imagetype)) return false;
        switch($imagetype)
        {
            case 'image/bmp': return '.bmp';
            case 'image/cis-cod': return '.cod';
            case 'image/gif': return '.gif';
            case 'image/ief': return '.ief';
            case 'image/jpeg': return '.jpg';
            case 'image/pipeg': return '.jfif';
            case 'image/tiff': return '.tif';
            case 'image/x-cmu-raster': return '.ras';
            case 'image/x-cmx': return '.cmx';
            case 'image/x-icon': return '.ico';
            case 'image/x-portable-anymap': return '.pnm';
            case 'image/x-portable-bitmap': return '.pbm';
            case 'image/x-portable-graymap': return '.pgm';
            case 'image/x-portable-pixmap': return '.ppm';
            case 'image/x-rgb': return '.rgb';
            case 'image/x-xbitmap': return '.xbm';
            case 'image/x-xpixmap': return '.xpm';
            case 'image/x-xwindowdump': return '.xwd';
            case 'image/png': return '.png';
            case 'image/x-jps': return '.jps';
            case 'image/x-freehand': return '.fh';
            default: return false;
        }
    }
}
