<?php

namespace App\Http\Controllers;

use App\Models\StripeTransactions;
use App\Models\StripeUsers;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class StripeController extends Controller
{
    private $stripe;

    public function __construct()
    {
        // Stripe initialization
        $this->stripe = new Stripe(config('stripe.secret_id'));
    }


    public function index(){
        return view('test');
    }

    /**
     * Function to authorize user and connect to the our Stripe account.
     *
     * @param Request $request
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     * @SWG\Post(
     *   path="/api/stripe/authorize",
     *   summary="Stripe authorize and connect",
     *   operationId="authorizeRestaurant",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="code",
     *     in="formData",
     *     description="Stripe code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="formData",
     *     description="Restaurant ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=422, description="Required field missing"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=400, description="bad request")
     * )
     *
     */
    public function authorizeRestaurant(Request $request)
    {
        if(!isset($request->code) || !isset($request->restaurant_id)){
            return response()->json(['status' => 'error', 'message' => 'Required fields are missing', 'code' => 422], 422);
        }

        $code = $request->code;

        $client = new Client();
        $res = $client->request('POST', 'https://connect.stripe.com/oauth/token', [
            'form_params' => [
                'client_secret' => config('stripe.secret_id'),
                'code' => $code,
                'grant_type' => 'authorization_code',
            ]
        ]);

        $response = json_decode($res->getBody());

        if(!empty($response)){
            if((new StripeUsers())->saveAction($request->restaurant_id, $response)){
                return response()->json(['status' => 'success', 'message' => 'Your account connected successfully!', 'code' => 200], 200);
            }
        }

        return response()->json(['status' => 'error', 'message' => 'Stripe error. Bad Request', 'code' => 400], 400);
    }

    /**
     * Create charge in Stripe
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/api/payment/charge",
     *   summary="Create charge in Stripe",
     *   operationId="chargeCreate",
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="formData",
     *     description="Restaurant ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="amount",
     *     in="formData",
     *     description="Amount",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="token",
     *     in="formData",
     *     description="Source (Card) token from Stripe",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=422, description="Required field missing"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=400, description="bad request")
     * )
     */
    public function chargeCreate(Request $request){
        if(!isset($request->restaurant_id)){
            return response()->json(['status' => 'error', 'message' => 'Restaurant is required', 'code' => 422], 422);
        }

        $restaurant_id = $request->restaurant_id;
        $amount = $request->amount;

        $stripe_partner_id = (new StripeUsers())->getAccountID($restaurant_id);

        $stripe_data = [
            "amount" => $amount/100,
            "currency" => "usd",
            "source" => $request->token,
//            "application_fee" => intval($amount/10),
            "application_fee" => 0,
            "destination" => [
                "account" => $stripe_partner_id,
            ]
        ];

        $charge = $this->stripe->charges()->create($stripe_data);

        if(!empty($charge)){
            $transaction_id = (new StripeTransactions())->saveAction($restaurant_id, $stripe_partner_id, $charge['id']);
            if($transaction_id){
                return response()->json(['status' => 'success', 'message' => 'Payment success', 'code' => 200, 'transaction_id' => $transaction_id], 200);
            }
        }

        return response()->json(['status' => 'error', 'message' => 'Stripe error. Bad Request', 'code' => 400], 400);
    }

    /**
     * Function to create refund in Stripe
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     *
     * @SWG\Post(
     *   path="/api/payment/refund",
     *   summary="Create refund in Stripe",
     *   operationId="refundCreate",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="transaction_id",
     *     in="formData",
     *     description="Transaction ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="amount",
     *     in="formData",
     *     description="Amount (in coints)",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=422, description="Required field missing"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=400, description="bad request")
     * )
     */
    public function refundCreate(Request $request){
        if(!$request->transaction_id || !$request->amount){
            return response()->json(['status' => 'error', 'message' => 'Transaction ID and amount is required', 'code' => 422], 422);
        }

        $amount = $request->amount/100;

        $this->doRefund($request->transaction_id, $amount);
    }

    /**
     * Do refund function
     *
     * @param $transaction_id
     * @param $amount
     * @return bool
     */
    public function doRefund($transaction_id, $amount){
        if(!$transaction_id || !$amount){
            return false;
        }

        $charge_id = (new StripeTransactions())->getChargeID($transaction_id);

        $this->stripe->refunds()->create($charge_id, $amount);
    }
}