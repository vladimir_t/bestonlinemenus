<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\AdminRestaurantController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/api/auth/login",
     *   summary="Login action",
     *   operationId="login",
     *   @SWG\Parameter(
     *     name="email",
     *     in="path",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     name="password",
     *     in="path",
     *     description="Password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=401, description="The user credentials were incorrect"),
     *   @SWG\Response(response=422, description="Could not create token"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    "error" => "invalid_credentials",
                    "message" => "The user credentials were incorrect.",
                    "code" => 401
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                "error" => "could_not_create_token",
                "message" => "Enable to process request.",
                "code" => 422
            ], 422);
        }

        // Get current user
        $user = JWTAuth::user();

        // Send login token info into Slack channel
        Log::error("Login token info", [
            'token' => $token,
            'decode_token' =>  JWTAuth::setToken($token)->getPayload()->toArray()
        ]);

        $dictionary = [];
        if($user->role == 'admin' || $user->role == 'restaurant') {
            $dictionary = (new AdminRestaurantController())->getDictionary();
        }else{
            return response()->json([
                "error" => "restrict_area",
                "message" => "Access deny. You can't access to this area with your type of user.",
                "code" => 403
            ], 403);
        }

        return response()->json([
            'user'  => $user,
            'token' => $token,
            'dictionary' => $dictionary,
            'code' => 200
        ],200);

    }
}
