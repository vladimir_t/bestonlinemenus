<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * @SWG\POST(
     *   path="/api/password/email",
     *   summary="Send reset password email action",
     *   operationId="getResetToken",
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="The user with this email was not found"),
     *   @SWG\Response(response=422, description="Required field missing"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getResetToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if($validator->fails()){
            if(!is_null($validator->errors())){
                $messge = '';
                foreach ($validator->errors()->all() as $msg){
                    $messge .= $msg . ' ';
                }
            }else{
                $messge = "Please fill all required fields!";
            }

            return response()->json(['status' => 'error', 'message' => trim($messge), 'code' => 422], 422);
        }

        if (isset($request->email)) {
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                return response()->json(['status' => 'error','message' => 'The user with this email was not found', 'code' => 404],404);
            }
            $this->sendResetLinkEmail($request);
            return response()->json(['status' => 'success', 'message' => 'Email with reset link sent successfully', 'code' => 200],200);
        }
    }
}
