<?php

namespace App\Http\Controllers;

use App\Models\Extra;
use App\Models\Pages;
use App\Models\RestaurantProfile;
use App\Models\Size;
use Validator;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\MenuSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JWTAuth;

class AdminMenuController extends Controller
{
    /**
     * Index menu action
     *
     * @SWG\Get(
     *   path="/api/menu",
     *   summary="List of menus",
     *   operationId="index",
     *
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function index()
    {
        $menus_list = (new Menu())->getAll();

        if($menus_list){
            return response()->json($menus_list, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Get menu info action
     *
     * @param $restaurant_id integer
     * @return string
     *
     * @SWG\Get(
     *   path="/api/menu/{restaurant_id}",
     *   summary="Menu info by restaurant id",
     *   operationId="getMenuInfo",
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant ID",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function getMenuInfo($restaurant_id)
    {
        if(!$restaurant_id){
            return response()->json(['status' => 'error', 'message' => 'Restaurant ID is required!', 'code' => 422], 422);
        }

        $menu_info = (new Menu())->getMenusByRestaurantId($restaurant_id);
        if($menu_info){
            return response()->json($menu_info, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * @param Request $request
     * @return string
     *
     * @SWG\Get(
     *   path="/api/category",
     *   summary="List of categories",
     *   operationId="getCategories",
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="query",
     *     description="Restaurant ID",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_url",
     *     in="query",
     *     description="Restaurant URL",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=403, description="Restrict area"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing")
     * )
     *
     */
    public function getCategories(Request $request){
        if(!isset($request->restaurant_id) && !isset($request->restaurant_url)){
            return response()->json(['status' => 'error', 'message' => 'Restaurant ID or URL is required!', 'code' => 422], 422);
        }

        if(!isset($request->restaurant_url)) {
            // Get current user and check if restaurant can have access
            $user = JWTAuth::parseToken()->authenticate();
            $profile_id = (new RestaurantProfile())->getProfileID($user->id);
            if ($user->role == 'restaurant' && isset($request->restaurant_id) && $profile_id != $request->restaurant_id) {
                return response()->json([
                    "error" => "restrict_area",
                    "message" => "Access deny. You can't access to this area with your type of user.",
                    "code" => 403
                ], 403);
            }
        }

        if($request->restaurant_id) {
            return response()->json((new MenuSection())->getSectionsByRestaurantId($request->restaurant_id), 200, [],JSON_NUMERIC_CHECK);
        }else{
            return response()->json((new Pages())->getSectionsByRestaurantUrl($request->restaurant_url), 200, [],JSON_NUMERIC_CHECK);
        }
    }


    /**
     * Function to get Category by ID
     *
     * @param Request $request
     * @return string
     *
     * @SWG\Get(
     *   path="/api/category/{id}",
     *   summary="Category by ID",
     *   operationId="getCategoryById",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Category ID",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing")
     * )
     *
     */
    public function getCategoryById($category_id){
        if(!$category_id){
            return response()->json(['status' => 'error', 'message' => 'Category ID is required!', 'code' => 422], 422);
        }

        return response()->json((new MenuSection())->getSectionById($category_id), 200,  [],JSON_NUMERIC_CHECK);
    }


    /**
     * Save category
     *
     * @param Request $request
     * @return string
     *
     * @SWG\Post(
     *   path="/api/category",
     *   summary="Save category action",
     *   operationId="saveCategory",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     * ),
     * @SWG\Parameter(
     *     name="restaurant_id",
     *     in="formData",
     *     description="Restaurant ID",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Category name",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="Category description",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="sizes",
     *     in="formData",
     *     description="Sizes json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="extra",
     *     in="formData",
     *     description="Extras json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="order",
     *     in="formData",
     *     description="Category order",
     *     required=false,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!"),
     *   @SWG\Response(response=422, description="Required field missing")
     * )
     *
     * @SWG\Patch(
     *   path="/api/category",
     *   summary="Update category action",
     *   operationId="updateCategory",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     * ),
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Category ID",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *     name="restaurant_id",
     *     in="formData",
     *     description="Restaurant ID",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Category name",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="Category description",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="sizes",
     *     in="formData",
     *     description="Sizes json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="extra",
     *     in="formData",
     *     description="Extras json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="order",
     *     in="formData",
     *     description="Category order",
     *     required=false,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!"),
     *   @SWG\Response(response=422, description="Required field missing")
     * )
     *
     */
    public function saveCategory(Request $request, $cat_id = null){
        // Validate request data
        if($request->method() == 'PATCH'){
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'restaurant_id' => 'required',
                'name' => 'required|max:255',
            ]);
        }

        if($validator->fails()){
            if(!is_null($validator->errors())){
                $messge = '';
                foreach ($validator->errors()->all() as $msg){
                    $messge .= $msg . ' ';
                }
            }else{
                $messge = "Please fill all required fields!";
            }

            return response()->json(['status' => 'error', 'message' => trim($messge), 'code' => 422], 422);
        }

        $category = (new MenuSection())->saveSection($cat_id, $request->all());

        if($category){
            return response()->json($category, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to delete menu category
     *
     * @param Request $request
     * @return false|string
     *
     * @SWG\Delete(
     *   path="/api/category",
     *   summary="Delete category action",
     *   operationId="deleteCategory",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Category id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function deleteCategory(Request $request){
        if(!isset($request->id)){
            return response()->json(['status' => 'error', 'message' => 'Section ID is required!', 'code' => 422], 422);
        }

        if((new MenuSection())->deleteSection($request->id)){
            return response()->json(['status' => 'success', 'message' => 'Section deleted successfully!', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Section not deleted! Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Function to get menu items
     *
     * @param Request $request
     * @return false|string
     *
     * @SWG\Get(
     *   path="/api/dish",
     *   summary="Dish list",
     *   operationId="getMenuItems",
     *   @SWG\Parameter(
     *     name="category_id",
     *     in="query",
     *     description="Category ID",
     *     required=false,
     *     type="integer"
     *   ),
     *
     *  @SWG\Parameter(
     *     name="category_name",
     *     in="query",
     *     description="Category name",
     *     required=false,
     *     type="string"
     *   ),
     *
     *  @SWG\Parameter(
     *     name="restaurant_url",
     *     in="query",
     *     description="Retsaurant URL",
     *     required=false,
     *     type="string"
     *   ),
     *
     *  @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     description="Dish name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Items per page",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="withSizes",
     *     in="query",
     *     description="Size flag",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="withExtras",
     *     in="query",
     *     description="Extras flag",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function getMenuItems(Request $request){
        if(!isset($request->category_id) && !isset($request->category_name)){
            return response()->json(['status' => 'error', 'message' => 'Category ID  or name is required!', 'code' => 422], 422);
        }

        $limit = 10;
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        $withSizes = false;
        $withExtras = false;

        if(isset($request->withSizes)){
            $withSizes = $request->withSizes;
        }

        if(isset($request->withExtras)){
            $withExtras = $request->withExtras;
        }

        if($request->category_id) {
            $menu_items = (new MenuItem())->getItems($request->category_id, $request->name, $limit, $withSizes, $withExtras);
        }elseif($request->category_name && $request->restaurant_url){
            $menu_items = (new MenuItem())->getItemsByCatName($request->category_name, $request->restaurant_url, $request->name, $limit, $withSizes, $withExtras);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Category ID or category name is wrong!', 'code' => 500], 500);
        }

        if($menu_items){
            return response()->json(['count' => $menu_items['total'], 'data' => $menu_items['data']], 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to get menu item (dish) by ID
     *
     * @param Request $request
     * @param $item_id
     * @return false|string
     *
     * @SWG\Get(
     *   path="/api/dish/{id}",
     *   summary="Dish by ID",
     *   operationId="getMenuItem",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Dish ID",
     *     required=true,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing")
     * )
     *
     */
    public function getMenuItem(Request $request, $item_id){
        if(!$item_id){
            return response()->json(['status' => 'error', 'message' => 'Dish ID is required!', 'code' => 422], 422);
        }

        return response()->json((new MenuItem())->getItemById($item_id), 200,[],JSON_NUMERIC_CHECK);
    }


    /**
     * Function to Create / Update menu item (dish)
     *
     * @param Request $request
     * @param $dish_id int
     * @return false|string
     *
     * @SWG\Post(
     *   path="/api/dish",
     *   summary="Save Dish action",
     *   operationId="saveMenuItem",
     *   consumes={"multipart/form-data"},
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     * ),
     * @SWG\Parameter(
     *     name="category_id",
     *     in="formData",
     *     description="Category ID",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Dish name",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="Dish description",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="price",
     *     in="formData",
     *     description="Dish price",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="picture",
     *     in="formData",
     *     description="Dish picture",
     *     required=false,
     *     type="file"
     *   ),
     * @SWG\Parameter(
     *     name="sizes",
     *     in="formData",
     *     description="Sizes json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="extra",
     *     in="formData",
     *     description="Extras json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="withSizes",
     *     in="formData",
     *     description="withSizes flag (true/false)",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     *
     * @SWG\Patch(
     *   path="/api/dish/{id}",
     *   summary="Update Dish action",
     *   operationId="updateMenuItem",
     *   consumes={"multipart/form-data"},
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     * ),
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Dish id",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *     name="category_id",
     *     in="formData",
     *     description="Category ID",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Dish name",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     description="Dish description",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="price",
     *     in="formData",
     *     description="Dish price",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="picture",
     *     in="formData",
     *     description="Dish picture",
     *     required=false,
     *     type="file"
     *   ),
     * @SWG\Parameter(
     *     name="sizes",
     *     in="formData",
     *     description="Sizes json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="extra",
     *     in="formData",
     *     description="Extras json array",
     *     required=false,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="withSizes",
     *     in="formData",
     *     description="withSizes flag (true/false)",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function saveMenuItem(Request $request, $dish_id = 0){

        $dish_id = $dish_id ? $dish_id : $request->id;

        // Validate request data
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required|max:255'
        ]);

        if($validator->fails()){
            if(!is_null($validator->errors())){
                $messge = '';
                foreach ($validator->errors()->all() as $msg){
                    $messge .= $msg . ' ';
                }
            }else{
                $messge = "Please fill all required fields!";
            }

            return response()->json(['status' => 'error', 'message' => trim($messge), 'code' => 422], 422);
        }

        $pic_path = '';

        if( $request->hasFile('picture') ) {
            $filename = $request->file('picture')->getClientOriginalName();
            $root = Storage::url('public/menu_item/picture/' . $request->section_id . '/');

            $exists = Storage::disk('local')->exists($root . $filename);
            if ($exists) {
                $delete = Storage::delete($root . $filename);
                if ($delete) {
                   Storage::putFileAs($root, $request->file('picture'), $filename);
                }
            } else {
                // If not exist, upload file into storage.
                Storage::putFileAs($root, $request->file('picture'), $filename);
            }

            $pic_path = env('APP_URL') . $root . $filename;
        }

        $menu_item = (new MenuItem())->saveItem($dish_id, $request->all(), $pic_path);
        if($menu_item){
            return response()->json($menu_item, 200,[],JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to delete menu item (dish)
     *
     * @param Request $request
     * @return false|string
     *
     * @SWG\Delete(
     *   path="/api/dish",
     *   summary="Delete dish action",
     *   operationId="deleteItemAction",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     description="Dish id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function deleteItemAction(Request $request){
        if(!isset($request->id)){
            return response()->json(['status' => 'error', 'message' => 'Dish ID is required!', 'code' => 422], 422);
        }

        if((new MenuItem())->deleteItem($request->id)){
            return response()->json(['status' => 'success', 'message' => 'Dish deleted successfully!', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Dish not deleted! Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Function to publish menu items (dish)
     *
     * @param Request $request
     * @return false|string
     *
     * @SWG\Post(
     *   path="/api/dish/publish",
     *   summary="Publish Dish action",
     *   operationId="publishMenuItems",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     * ),
     * @SWG\Parameter(
     *     name="dish_items",
     *     in="formData",
     *     description="Dish id's json array. Example {'dish_items': [1,2,3]}",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     */
    public function publishMenuItems(Request $request){
        if(!$request->dish_items){
            return response()->json(['status' => 'error', 'message' => 'Dish items array is empty!', 'code' => 422], 422);
        }

        if((new MenuItem())->publishItems($request->dish_items, "true")) {
            return response()->json(['status' => 'success', 'msg' => 'Items published successfully', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Items not published! Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to unpublish menu items (dish)
     *
     * @param Request $request
     * @return false|string
     *
     * @SWG\Post(
     *   path="/api/dish/unpublish",
     *   summary="Unpublish Dish action",
     *   operationId="unpublishMenuItems",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     * ),
     * @SWG\Parameter(
     *     name="dish_items",
     *     in="formData",
     *     description="Dish id's json array. Example {'dish_items': [1,2,3]}",
     *     required=false,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     */
    public function unpublishMenuItems(Request $request){
        if(!$request->dish_items){
            return response()->json(['status' => 'error', 'message' => 'Dish items array is empty!', 'code' => 422], 422);
        }

        if((new MenuItem())->publishItems($request->dish_items, "false")) {
            return response()->json(['status' => 'success', 'message' => 'Item unpublished successfully', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Items not unpublished! Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to update dish ordering
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PATCH(
     *   path="/api/sort/dish",
     *   summary="Update dish ordering",
     *   operationId="updateDishOrder",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="items",
     *     in="formData",
     *     description="Items json array. Example [{'id':53, 'order':1}, {'id':54, 'order':2}]",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function updateDishOrder(Request $request){
        if(!$request->items){
            return response()->json(['status' => 'error', 'message' => 'Items array is empty!', 'code' => 422], 422);
        }

        $new_items = (new MenuItem())->updateOrdering($request->items);
        if($new_items){
            return response()->json(['status' => 'success', 'message' => 'Items updated successfully', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to update categories ordering
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PATCH(
     *   path="/api/sort/category",
     *   summary="Update categories ordering",
     *   operationId="updateCategoryOrder",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="items",
     *     in="formData",
     *     description="Items json array. Example [{'id':53, 'order':1}, {'id':54, 'order':2}]",
     *     required=true,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     */
    public function updateCategoryOrder(Request $request){
        if(!$request->items){
            return response()->json(['status' => 'error', 'message' => 'Items array is empty!', 'code' => 422], 422);
        }

        $new_categories = (new MenuSection())->updateOrdering($request->items);
        if($new_categories){
            return response()->json(['status' => 'success', 'message' => 'Categories updated successfully', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Function to save category Extra
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     *  @SWG\PATCH(
     *   path="/api/category/{id}/extras",
     *   summary="Save/Update categories extra",
     *   operationId="saveExtras",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     type="number",
     *     in="path",
     *     description="Category ID",
     *     required=true
     *   ),
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="List json array.",
     *     required=true,
     *     @SWG\Schema(
     *      @SWG\Property(
     *          property="extra_lists",
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="number"),
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="multi_selection", type="string"),
     *              @SWG\Property(
     *                  property="items",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="id", type="number"),
     *                      @SWG\Property(property="name", type="string"),
     *                      @SWG\Property(property="price", type="number"),
     *                      )
     *                  )
     *              )
     *          ),
     *       @SWG\Property(
     *       property="extras",
     *       type="array",
     *       @SWG\Items(
     *         type="object",
     *          @SWG\Property(property="id", type="number"),
     *          @SWG\Property(property="name", type="string"),
     *          @SWG\Property(property="price", type="number"),
     *          )
     *        )
     *      ),
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function saveExtras(Request $request){
        $data = $request->all();
        $cat_id = $request->id;

        if(empty($data)){
            return response()->json(['status' => 'error', 'message' => 'Extras data is empty!', 'code' => 422], 422);
        }

        $response = (new Extra())->saveExtraData($data, $cat_id);
        if($response){
            return response()->json($response, 200, [],JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Function to save item sizes.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PATCH(
     *   path="/api/dish/{id}/sizes",
     *   summary="Save/Update dish sizes",
     *   operationId="saveDishSize",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     type="number",
     *     in="path",
     *     description="Dish ID",
     *     required=true
     *   ),
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Sizes json.",
     *     required=true,
     *     @SWG\Schema(
     *      @SWG\Property(property="category_id", type="number"),
     *      @SWG\Property(
     *          property="items",
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="number"),
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="price", type="number")
     *              )
     *          ),
     *      ),
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     *
     */
    public function saveDishSize(Request $request){
        $data = $request->all();
        $item_id = $request->id;

        if(empty($data)){
            return response()->json(['status' => 'error', 'message' => 'Size data is empty!', 'code' => 422], 422);
        }

        $response = (new Size())->saveSizeData($data, $item_id);
        if($response){
            return response()->json($response, 200, [],JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Function to save dish extra
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PATCH(
     *   path="/api/dish/{id}/extra",
     *   summary="Save/Update dish extra",
     *   operationId="saveDishExtra",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     type="number",
     *     in="path",
     *     description="Dish ID",
     *     required=true
     *   ),
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="List json array.",
     *     required=true,
     *     @SWG\Schema(
     *      @SWG\Property(
     *          property="extra_lists",
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="number"),
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="multi_selection", type="string"),
     *              @SWG\Property(
     *                  property="items",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="id", type="number"),
     *                      @SWG\Property(property="name", type="string"),
     *                      @SWG\Property(property="price", type="number"),
     *                      )
     *                  )
     *              )
     *          ),
     *       @SWG\Property(
     *       property="extras",
     *       type="array",
     *       @SWG\Items(
     *         type="object",
     *          @SWG\Property(property="id", type="number"),
     *          @SWG\Property(property="name", type="string"),
     *          @SWG\Property(property="price", type="number"),
     *          )
     *        )
     *      ),
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function saveDishExtra(Request $request){
        $data = $request->all();
        $item_id = $request->id;

        if(empty($data)){
            return response()->json(['status' => 'error', 'message' => 'Extra data is empty!', 'code' => 422], 422);
        }

        $response = (new Extra())->updateExtra($data, $item_id);
        if($response){
            return response()->json($response, 200, [],JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Get Extra info by category ID
     *
     * @param $item_id
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/api/extras/{item_id}",
     *   summary="Extra by item ID",
     *   operationId="getAllExtra",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Item ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing")
     * )
     *
     */
    public function getAllExtra($item_id){
        if(!$item_id){
            return response()->json(['status' => 'error', 'message' => 'Category ID is required!', 'code' => 422], 422);
        }

        $extras = (new Extra())->getAllByItemId($item_id);
        if($extras){
            return response()->json($extras, 200, [],JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Get Extra info by category ID
     *
     * @param $item_id
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/api/sizes/{item_id}",
     *   summary="Sizes by item ID",
     *   operationId="getAllSize",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Item ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="success"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=422, description="required field missing")
     * )
     *
     */
    public function getAllSize($item_id){
        if(!$item_id){
            return response()->json(['status' => 'error', 'message' => 'Category ID is required!', 'code' => 422], 422);
        }

        $size = (new Size())->getAllByItemId($item_id);
        if($size){
            return response()->json($size, 200, [],JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }
}
