<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use App\Models\Zipcodes;
use Validator;
use App\Models\CuisineType;
use App\Models\RestaurantProfile;
use App\Models\SalesAgents;
use Illuminate\Http\Request;
use JWTAuth;

class AdminRestaurantController extends Controller
{
    private $cuisineList;


    public function __construct()
    {
        $this->cuisineList = (new CuisineType())->getAll();
    }

    /**
     * Index action
     *
     * @param Request $request
     * @return false|string
     *
     * @SWG\Get(
     *   path="/api/restaurant",
     *   summary="List of restaurants",
     *   operationId="index",
     *  @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="revenue",
     *     in="path",
     *     description="Revenue filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="salesAgent",
     *     in="path",
     *     description="Sales Agent filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="path",
     *     description="Status filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="subscription",
     *     in="path",
     *     description="Subscription filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="cuisine",
     *     in="path",
     *     description="Cuisine filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="search",
     *     in="path",
     *     description="Search filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="onboardingPeriodStart",
     *     in="path",
     *     description="Onboarding Period Start",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="onboardingPeriodEnd",
     *     in="path",
     *     description="Onboarding Period End",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sort_order",
     *     in="path",
     *     description="sort order filter",
     *     required=false,
     *     enum={"desc", "asc"},
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sort_field",
     *     in="path",
     *     description="sort field filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="path",
     *     description="limit rows per page",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="Restaurans list empty")
     * )
     *
     */
    public function index(Request $request)
    {
        //Get all profiles from DB
        $restaurantProfileModel = new RestaurantProfile();
        $restaurants_list = $restaurantProfileModel->getAllProfiles($request);

        if(!empty($restaurants_list)){
            return response()->json(['count' => $restaurants_list['total'], 'data' => $restaurants_list['data'], 'code' => 200], 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Restaurans list empty', 'code' => 404], 404);
    }

    /**
     * Function to get user deictionary array
     *
     * @return array|bool
     *
     * @SWG\Get(
     *   path="/api/dictionary",
     *   summary="Get dictionary list",
     *   operationId="getDictionary",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getDictionary(){
        return (new RestaurantProfile())->getDictionary();
    }

    /**
     * Get restaurant profile action
     *
     * @param Request $request
     * @param $profile_id
     * @return string
     *
     * @SWG\Get(
     *   path="/api/restaurant/{id}",
     *   summary="Get restaurant by id or restarant_url",
     *   operationId="getProfileAction",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Profile id",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="isClient",
     *     in="query",
     *     description="isClient flag (bool)",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=406, description="is blocked"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getProfileAction(Request $request, $profile_id){
        //Check if Restaurant
        $isClient = false;
        if(isset($request->isClient)){
            $isClient = true;
        }

        if(is_numeric($profile_id) && !$isClient) {
            //Get profile from DB by ID
            $restaurant_profile = (new RestaurantProfile())->getProfileById($profile_id, $isClient);
        }elseif(!is_numeric($profile_id) && $isClient){
            //Get profile from DB by URL name
            $restaurant_profile = (new Pages())->getProfileByUrl($profile_id, $isClient);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Invalid ID parametr', 'code' => 500], 500);
        }

        if($restaurant_profile){
            if($isClient && $restaurant_profile->status == 'blocked'){
                return response()->json(['status' => 'error', 'message' => 'Sorry, this restaurant is blocked!', 'code' => 406], 406);
            }
            return response()->json($restaurant_profile, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Restaurant not found', 'code' => 404], 404);
    }

    /**
     * Block/activate restaurant action
     *
     * @param Request $request
     * @return string
     *
     * @SWG\Post(
     *   path="/api/restaurant/block",
     *   summary="Block/activate restaurant action",
     *   operationId="blockUnblockAction",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Profile id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="path",
     *     description="Status (active/blocked)",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=422, description="Required field missing or duplicate record"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="internal server error")
     * )
     *
     */
    public function blockUnblockAction(Request $request){
        if(!$request->id){
            return response()->json(['status' => 'error', 'message' => 'Id is required', 'code' => 422], 422);
        }

        $update_status = 'active';
        if($request->status != 'blocked'){
            $update_status = 'blocked';
        }

        $profile = (new RestaurantProfile())->saveProfile($request->id, ['status' => $update_status], true);

        if($profile) {
            return response()->json($profile, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support! Status not changed', 'code' => 522], 522);
    }

    /**
     * Save restaurant profile action
     *
     * @param Request $request
     * @param $id int
     * @return string
     *
     * @SWG\Post(
     *   path="/api/restaurant",
     *   summary="Save restaurant action",
     *   operationId="saveAction",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="agent_name",
     *     in="formData",
     *     description="Agent name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_name",
     *     in="formData",
     *     description="Restaurant name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="manager_name",
     *     in="formData",
     *     description="Manager name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="manager_email",
     *     in="formData",
     *     description="Manager email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_email",
     *     in="formData",
     *     description="Delivery email",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="cuisine_type",
     *     in="formData",
     *     description="Cuisine type",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="street_address",
     *     in="formData",
     *     description="Address",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone_number",
     *     in="formData",
     *     description="Restaurant Phone Number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sales_tax",
     *     in="formData",
     *     description="Sales Tax",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_method",
     *     in="formData",
     *     description="Delivery Method",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_tax",
     *     in="formData",
     *     description="Delivery tax",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_fee",
     *     in="formData",
     *     description="Delivery fee",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="subscription",
     *     in="formData",
     *     description="Subscription",
     *     required=true,
     *     enum={"1 year", "2 years", "monthly"},
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="tags",
     *     in="formData",
     *     description="Keywords",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="Zipcode",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="City",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state",
     *     in="formData",
     *     description="State",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="hours",
     *     in="formData",
     *     description="Hours JSON array can be add in PATCH request",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     * @SWG\Patch(
     *   path="/api/restaurant",
     *   summary="Update restaurant action",
     *   operationId="updateAction",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Profile id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="agent_name",
     *     in="formData",
     *     description="Agent name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_name",
     *     in="formData",
     *     description="Restaurant name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="manager_name",
     *     in="formData",
     *     description="Manager name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="manager_email",
     *     in="formData",
     *     description="Manager email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_email",
     *     in="formData",
     *     description="Delivery email",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="cuisine_type",
     *     in="formData",
     *     description="Cuisine type",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="street_address",
     *     in="formData",
     *     description="Address",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone_number",
     *     in="formData",
     *     description="Restaurant Phone Number",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sales_tax",
     *     in="formData",
     *     description="Sales Tax",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_method",
     *     in="formData",
     *     description="Delivery Method",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_tax",
     *     in="formData",
     *     description="Delivery tax",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_fee",
     *     in="formData",
     *     description="Delivery fee",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="subscription",
     *     in="formData",
     *     description="Subscription",
     *     required=true,
     *     enum={"1 year", "2 years", "monthly"},
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="tags",
     *     in="formData",
     *     description="Keywords",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="Zipcode",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="City",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state",
     *     in="formData",
     *     description="State",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="hours",
     *     in="formData",
     *     description="Hours JSON array can be add in PATCH request",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=422, description="Missing required field or duplicate records"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="Something is wrong, please contact support!")
     * )
     *
     */
    public function saveAction(Request $request, $id = 0){
        $data = $request->all();

        // We don't need validate fields, when setting up hours
        if(!isset($data['hours'])) {

            // Validate request data
            $validator = Validator::make($data, [
                'restaurant_name' => 'required|max:255',
                'manager_name' => 'required|max:255',
                'manager_email' => 'required|email|max:255',
                'subscription' => 'required',
                'street_address' => 'required|max:255',
                'zipcode' => 'required',
                'city' => 'required|max:255',
                'state' => 'required|max:255'
            ]);

            if($validator->fails()){
                if(!is_null($validator->errors())){
                    $messge = '';
                    foreach ($validator->errors()->all() as $msg){
                        $messge .= $msg . ' ';
                    }
                }else{
                    $messge = "Please fill all required fields!";
                }

                return response()->json(['status' => 'error', 'message' => trim($messge), 'code' => 422], 422);
            }
        }

        $returnList = false;
        if($request->getMethod() == 'PATCH' && JWTAuth::user()->isAdmin()){
            $returnList = true;
        }

        $profile = (new RestaurantProfile())->saveProfile($id, $data, $returnList);

        if($profile){
            return response()->json($profile, 200, [], JSON_NUMERIC_CHECK);
        }

        return response()->json(['status' => 'error', 'message' => 'Something is wrong, please contact support!', 'code' => 522], 522);
    }


    /**
     * Delete restaurant profile action
     *
     * @param Request $request
     * @return bool|false|string
     *
     * @SWG\Delete(
     *   path="/api/restaurant",
     *   summary="Delete restaurant action",
     *   operationId="deleteAction",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Profile id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=522, description="something is wrong, please contact support"),
     *   @SWG\Response(response=422, description="missing required field")
     * )
     */
    public function deleteAction($profile_id){
        if(!$profile_id){
            return response()->json(['status' => 'error', 'message' => 'Restaurant ID is required!', 'code' => 422], 422);
        }

        if((new RestaurantProfile())->deleteProfile($profile_id)){
            return response()->json(['status' => 'success', 'message' => 'Restaurant profile deleted successfully', 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Restaurant profile not deleted. Something is wrong, please contact support!', 'code' => 522], 522);
    }

    /**
     * Function to get list of Sales Agents
     * @param Request $request
     *
     * @return string
     *
     *  @SWG\GET(
     *   path="/api/agent",
     *   summary="Get list of Sales Agents",
     *   operationId="getAgents",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     description="Filter by name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Page limit",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     */
    public function getAgents(Request $request){
        $limit = 10;
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        if(isset($request->name)){
            $agents = (new SalesAgents())->getAllAgents($request->name, $limit);
        }else{
            $agents = (new SalesAgents())->getAllAgents(false, $limit);
        }

        return response()->json($agents, 200);
    }

    /**
     * @param Request $request
     * @return false|\Illuminate\Http\JsonResponse|string
     *
     *  @SWG\Get(
     *   path="/api/zipcode",
     *   summary="Search by zipcode or city",
     *   operationId="getAddressInfo",
     *   @SWG\Parameter(
     *     name="search",
     *     in="query",
     *     description="Search filter",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Page limit",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     required=false,
     *     type="integer"
     *   ),
     *
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="Restaurans list empty"),
     *   @SWG\Response(response=422, description="Required field is empty")
     * )
     *
     */
    public function getAddressInfo(Request $request){
        $result = (new Zipcodes())->getInfoByFilter($request->search, 10);

        if($result){
            return response()->json(['count' => $result['total'], 'data' => $result['data'], 'code' => 200], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Address not found!', 'code' => 404], 404);
    }


    /**
     * Function to get list of cuisine types
     * @param Request $request
     *
     * @return string
     *
     *  @SWG\GET(
     *   path="/api/cuisine",
     *   summary="Get list of cuisine types",
     *   operationId="getCuisine",
     *   @SWG\Parameter(
     *    name="Authorization",
     *    in="header",
     *    description="an authorization header",
     *    required=true,
     *    type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     description="Filter by name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Page limit",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     */
    public function getCuisine(Request $request){
        $limit = 10;
        if(isset($request->limit)){
            $limit = $request->limit;
        }

        $name_filter = false;
        if(isset($request->name)){
            $name_filter = $request->name;
        }

        $cuisine_types = (new CuisineType())->getByFilter($name_filter, $limit);

        return response()->json($cuisine_types, 200);
    }
}
