<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Illuminate\Http\Request;

class Restaurant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(JWTAuth::user()->isRestaurant()) {
            return $next($request);
        }

        return response()->json([
            "error" => "restrict_area",
            "message" => "Access deny. You can't access to this area with your type of user.",
            "code" => 403
        ], 403);
    }
}
