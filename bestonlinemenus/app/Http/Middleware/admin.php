<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Illuminate\Http\Request;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {

        if(JWTAuth::user()->isAdmin()) {
            return $next($request);
        }

        return response()->json([
            "error" => "restrict_area",
            "message" => "Access deny. You can't access to this area with your type of user.",
            "code" => 403
        ], 403);
    }
}
