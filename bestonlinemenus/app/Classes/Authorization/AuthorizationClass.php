<?php

namespace App\Classes\Authorization;

use Dlnsk\HierarchicalRBAC\Authorization;


/**
 *  This is example of hierarchical RBAC authorization configiration.
 */

class AuthorizationClass extends Authorization
{
	public function getPermissions() {
		return [
			'editRestaurant' => [
					'description' => 'Create / Edit restaurant',
//					'next' => 'editReustarantProfile',
				],
            'deleteRestaurant' => [
                'description' => 'Delete restaurant',
            ],

			'editRestaurantProfile' => [
					'description' => 'Create / Edit restaurant profile',
            ],

			'deleteRestaurantProfile' => [
					'description' => 'Delete restaurant profile',
            ],

            'editMenu' => [
                'description' => 'Create / Edit restaurant menu',
            ],

            'deleteMenu' => [
                'description' => 'Delete restaurant menu',
            ],

            'editMenuItem' => [
                'description' => 'Create / Edit restaurant menu item',
            ],

            'deleteMenuItem' => [
                'description' => 'Delete restaurant menu item',
            ],

            'addSize' => [
                'description' => 'Add size for the menu item',
            ],

            'removeSize' => [
                'description' => 'Remove size for the menu item',
            ],

            'addExtars' => [
                'description' => 'Add extars for the menu item',
            ],

            'removeExtars' => [
                'description' => 'Remove extars for the menu item',
            ]
		];
	}

	public function getRoles() {
		return [
			'restaurant' => [
					'editRestaurantProfile',
					'deleteRestaurantProfile',
					'editMenu',
					'deleteMenu',
					'editMenuItem',
					'deleteMenuItem',
					'addSize',
					'removeSize',
					'addExtars',
					'removeExtars'
            ],
			'user' => [

            ],
		];
	}

}
