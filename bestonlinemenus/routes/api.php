<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'password'],function() {
    Route::post('/email', 'Auth\ForgotPasswordController@getResetToken')->middleware('api');
    Route::post('/reset', 'Auth\ResetPasswordController@reset')->middleware('api')->name('password.reset');
});

Route::group(['prefix'=> 'auth'],function(){
    Route::post("/login",'Auth\LoginController@login')->middleware('api');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Restaurants list
//Route::get('restaurants', 'AdminRestaurantController@index')->name('restaurants')->middleware('auth', 'admin');
Route::get('restaurant', 'AdminRestaurantController@index')->middleware('jwt.auth', 'api', 'admin')->name('restaurants');
Route::options('restaurant', 'AdminRestaurantController@index')->middleware('jwt.auth', 'api', 'admin')->name('restaurants');

//Save restaurant
Route::post('restaurant', 'AdminRestaurantController@saveAction')->middleware('jwt.auth', 'api', 'superroles');
Route::patch('restaurant/{id}', 'AdminRestaurantController@saveAction')->middleware('jwt.auth', 'api', 'superroles');

//Get restaurant
Route::get('/restaurant/{id}', 'AdminRestaurantController@getProfileAction')->name('profile')->middleware('api');
Route::get('/restaurant/{restarant_url}', 'AdminRestaurantController@getProfileAction')->name('profile')->middleware('api');

//Change restaurant status
Route::post('restaurant/block', 'AdminRestaurantController@blockUnblockAction')->middleware('jwt.auth', 'api', 'admin');

//Delete restaurant
Route::delete('restaurant/{id}', 'AdminRestaurantController@deleteAction')->middleware('jwt.auth', 'api', 'admin');

// Dictionary
Route::get('dictionary', 'AdminRestaurantController@getDictionary')->middleware('jwt.auth', 'api', 'superroles')->name('dictionary');
Route::options('dictionary', 'AdminRestaurantController@getDictionary')->middleware('jwt.auth', 'api', 'superroles')->name('dictionary');

// Sales Agents
Route::get('agent', 'AdminRestaurantController@getAgents')->middleware('api')->name('agent');
Route::options('agent', 'AdminRestaurantController@getAgents')->middleware('api')->name('agent');

// Get cuisine
Route::get('cuisine', 'AdminRestaurantController@getCuisine')->middleware('api')->name('cuisine');
Route::options('cuisine', 'AdminRestaurantController@getCuisine')->middleware('api')->name('cuisine');

// Menu info
Route::get('menu', 'AdminMenuController@index')->middleware('api')->name('menu');
Route::get('menu/{restaurant_id}', 'AdminMenuController@getMenuInfo')->middleware('api')->name('menu');

// Categories list (Menu categories)
Route::get('category', 'AdminMenuController@getCategories')->middleware('api')->name('category');

// Get category by id
Route::get('category/{id}', 'AdminMenuController@getCategoryById')->middleware('jwt.auth', 'api', 'superroles')->name('category');

// Save / Update category
Route::post('category', 'AdminMenuController@saveCategory')->middleware('jwt.auth', 'api', 'superroles')->name('category');
Route::patch('category/{id}', 'AdminMenuController@saveCategory')->middleware('jwt.auth', 'api', 'superroles')->name('category');

// Update category order
Route::patch('sort/category', 'AdminMenuController@updateCategoryOrder')->middleware('jwt.auth', 'api', 'superroles')->name('sort');

// Delete category
Route::delete('category/{id}', 'AdminMenuController@deleteCategory')->middleware('jwt.auth', 'api', 'superroles')->name('category');

// Save / Update extra
Route::patch('category/{id}/extras/', 'AdminMenuController@saveExtras')->middleware('jwt.auth', 'api', 'superroles')->name('category');

// Get dish list
Route::get('dish', 'AdminMenuController@getMenuItems')->middleware('api')->name('dish');

// Get dish by id
Route::get('dish/{id}', 'AdminMenuController@getMenuItem')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Save / Update dish item
Route::post('dish', 'AdminMenuController@saveMenuItem')->middleware('jwt.auth', 'api', 'superroles')->name('dish');
Route::post('dish/{id}', 'AdminMenuController@saveMenuItem')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Update dish order
Route::patch('sort/dish', 'AdminMenuController@updateDishOrder')->middleware('jwt.auth', 'api', 'superroles')->name('sort');

// Publish dish
Route::post('dish/publish', 'AdminMenuController@publishMenuItems')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Unpublish
Route::post('dish/unpublish', 'AdminMenuController@unpublishMenuItems')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Delete dish
Route::delete('dish/{id}', 'AdminMenuController@deleteItemAction')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Save / Update size
Route::patch('dish/{id}/sizes', 'AdminMenuController@saveDishSize')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Save / Update dish extra
Route::patch('dish/{id}/extras', 'AdminMenuController@saveDishExtra')->middleware('jwt.auth', 'api', 'superroles')->name('dish');

// Get page designer info
Route::get('designer/{restaurant_id}', 'AdminPageDesignerController@getPageInfo')->middleware('jwt.auth', 'api', 'superroles')->name('designer');

// Update designer dish
Route::post('designer', 'AdminPageDesignerController@savePageAction')->middleware('jwt.auth', 'api', 'superroles')->name('designer');
Route::patch('designer/{restaurant_id}', 'AdminPageDesignerController@savePageAction')->middleware('jwt.auth', 'api', 'superroles')->name('designer');

// Get address info
Route::get('zipcode', 'AdminRestaurantController@getAddressInfo')->middleware('api')->name('designer');

//Stripe authorize request
Route::post('stripe/authorize', 'StripeController@authorizeRestaurant')->middleware('api')->name('stripe');

//Stripe charge request
Route::post('payment/charge', 'StripeController@chargeCreate')->middleware('api')->name('stripe');

//Stripe refund change
Route::post('payment/refund', 'StripeController@refundCreate')->middleware('jwt.auth', 'api', 'superroles')->name('refund');

// Post order info
Route::post('order', 'OrderController@saveOrderAction')->middleware('api')->name('order');
Route::patch('order/{id}', 'OrderController@saveOrderAction')->middleware('jwt.auth', 'api', 'superroles')->name('order');

// Update order products
Route::patch('product', 'OrderController@updateOrderProductsAction')->middleware('jwt.auth', 'api', 'superroles')->name('product');

// Get orders list
Route::get('order', 'OrderController@getOrdersList')->middleware('jwt.auth', 'api', 'superroles')->name('order');

// Get order info by id
Route::get('order/{id}', 'OrderController@getOrderById')->middleware('jwt.auth', 'api', 'superroles')->name('order');

// Delete order by id
Route::delete('order/{id}', 'OrderController@deleteOrder')->middleware('jwt.auth', 'api', 'superroles')->name('order');

// Get total revenue and total orders completed info
Route::get('order/total/{restaurant_id}', 'OrderController@getOrdersTotal')->middleware('jwt.auth', 'api', 'superroles')->name('order');

// Get all extras by category ID
Route::get('extras/{item_id}', 'AdminMenuController@getAllExtra')->middleware('jwt.auth', 'api', 'superroles')->name('extras');

// Get all sizes by category ID
Route::get('sizes/{item_id}', 'AdminMenuController@getAllSize')->middleware('jwt.auth', 'api', 'superroles')->name('sizes');