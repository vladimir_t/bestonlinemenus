<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('sales_agents', 'sales_agents_relation');

        Schema::table('sales_agents_relation', function(Blueprint $table)
        {
            $table->dropColumn('agent_name');
            $table->integer('agent_id')->nullable();
            $table->unique(['restaurant_id', 'agent_id'], 'unique');
        });

        Schema::create('sales_agents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('agent_name', 256)->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
