<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_profile', function(Blueprint $table)
        {
            $table->dropColumn('menu_id');
        });

        Schema::table('menus', function(Blueprint $table)
        {
            $table->integer('restaurant_id')->after('id');
            $table->dropColumn('status');
            $table->dropColumn('date_saved');
            $table->dropColumn('date_published');
        });

        Schema::table('menu_section', function(Blueprint $table)
        {
            $table->dropColumn('date_saved');
            $table->dropColumn('date_published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
