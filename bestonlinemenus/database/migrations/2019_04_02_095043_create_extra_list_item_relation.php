<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraListItemRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_list_item_relation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('list_id')->unsigned();
            $table->integer('item_id')->unsigned();
        });

        Schema::table('extra_list_item_relation', function (Blueprint $table) {
            DB::statement('ALTER TABLE `extra_list_item_relation` ADD FOREIGN KEY (`list_id`) REFERENCES `extra_lists`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
            DB::statement('ALTER TABLE `extra_list_item_relation` ADD FOREIGN KEY (`item_id`) REFERENCES `menu_item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_list_item_relation');
    }
}
