<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('restaurant_id')->unique();
            $table->string('stripe_user_id');
            $table->string('stripe_publishable_key');
            $table->string('access_token');
            $table->string('refresh_token');
            $table->timestamps();
        });

        Schema::table('stripe_users', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `stripe_users` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_users');
    }
}
