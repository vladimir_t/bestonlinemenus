<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbKeyUpdates extends Migration
{

    public function __construct()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_profile', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `restaurant_profile` ADD FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('sales_agents_relation', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `sales_agents_relation` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('menus', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `menus` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('pages', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `pages` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('order', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `order` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('working_hours_relation', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `working_hours_relation` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('order_products', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `order_products` ADD FOREIGN KEY (`order_id`) REFERENCES `order`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('menu_section', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `menu_section` ADD FOREIGN KEY (`menu_id`) REFERENCES `menus`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('menu_item', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `menu_item` ADD FOREIGN KEY (`section_id`) REFERENCES `menu_section`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('menu_item_extra', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `menu_item_extra` ADD FOREIGN KEY (`item_id`) REFERENCES `menu_item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

        Schema::table('menu_item_size', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `menu_item_size` ADD FOREIGN KEY (`item_id`) REFERENCES `menu_item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
