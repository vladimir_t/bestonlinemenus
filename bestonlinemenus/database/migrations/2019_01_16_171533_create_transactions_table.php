<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('restaurant_id')->unique();
            $table->string('stripe_user_id');
            $table->string('stripe_transaction_id');
            $table->timestamp('created_at');
        });

        Schema::table('transactions', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `transactions` ADD FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_profile`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
