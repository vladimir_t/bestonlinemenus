<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->integer('transaction_id')->nullable()->after('total_price');
        });

        Schema::table('order_products', function (Blueprint $table) {
            $table->dropColumn('selectedExtra');
            $table->integer('amount')->nullable()->after('selectedSize');
            $table->float('total_price')->nullable()->after('amount');
        });

        Schema::create('order_products_extra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('extra_id');
            $table->integer('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
