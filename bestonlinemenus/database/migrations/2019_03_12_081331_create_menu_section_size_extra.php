<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuSectionSizeExtra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('menu_item_extra', 'extra');
        Schema::rename('menu_item_size', 'size');

        Schema::table('extra', function (Blueprint $table) {
            $table->integer('section_id')->nullable()->after('id');
            $table->integer('list_id')->nullable()->after('section_id');
            $table->integer('size_id')->nullable()->after('price');
        });

        Schema::table('size', function (Blueprint $table) {
            $table->integer('section_id')->nullable()->after('id');
        });

        Schema::create('extra_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('section_id');
            $table->enum('multi_selection', ['true', 'false'])->default('false');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
