<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id');
            $table->string('name', 256);
            $table->string('phone', 20);
            $table->string('city', 256);
            $table->string('street_address', 256);
            $table->integer('building');
            $table->integer('appartment');
            $table->string('zipcode', 5);
            $table->string('payment_method', 256);
            $table->enum('status', ['new', 'pending', 'in_process', 'complete', 'refund'])->default('new');
            $table->timestamp('date_created')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
