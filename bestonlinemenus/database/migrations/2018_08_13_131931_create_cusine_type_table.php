<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCusineTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisine_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cuisine_name', 256);
        });
        
        \Illuminate\Support\Facades\DB::table('cuisine_type')
            ->insert([
                    ["cuisine_name" => "African"],
                    ["cuisine_name" => "American"],
                    ["cuisine_name" => "Asian"],
                    ["cuisine_name" => "Belgian & Dutch"],
                    ["cuisine_name" => "British"],
                    ["cuisine_name" => "Cajun & Creole"],
                    ["cuisine_name" => "Caribbean"],
                    ["cuisine_name" => "Chinese"],
                    ["cuisine_name" => "Cuban"],
                    ["cuisine_name" => "Eastern European"],
                    ["cuisine_name" => "French"],
                    ["cuisine_name" => "German, Austrian & Swiss"],
                    ["cuisine_name" => "Greek"],
                    ["cuisine_name" => "Indian"],
                    ["cuisine_name" => "Irish"],
                    ["cuisine_name" => "Italian"],
                    ["cuisine_name" => "Japanese"],
                    ["cuisine_name" => "Korean"],
                    ["cuisine_name" => "Latin American"],
                    ["cuisine_name" => "Mediterranean"],
                    ["cuisine_name" => "Mexican"],
                    ["cuisine_name" => "Middle Eastern"],
                    ["cuisine_name" => "Moroccan"],
                    ["cuisine_name" => "Polish"],
                    ["cuisine_name" => "Portuguese"],
                    ["cuisine_name" => "Scandinavian"],
                    ["cuisine_name" => "Southeast Asian"],
                    ["cuisine_name" => "Southern/Soul Food"],
                    ["cuisine_name" => "Southwestern/Tex-Mex"],
                    ["cuisine_name" => "Spanish"],
                    ["cuisine_name" => "Thai"],
                    ["cuisine_name" => "Turkish"],
                    ["cuisine_name" => "Vietnamese"]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuisine_type');
    }
}
