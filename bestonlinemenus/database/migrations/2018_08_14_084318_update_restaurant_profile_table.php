<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRestaurantProfileTable extends Migration
{

    public function __construct()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_profile', function(Blueprint $table)
        {
            $table->string('delivery_email')->nullable()->change();
            $table->string('phone_number2')->nullable()->change();
            $table->string('sales_tax')->nullable()->change();
            $table->string('working_hours')->nullable()->change();
            $table->string('away_hours')->nullable()->change();
            $table->string('pickup_hours')->nullable()->change();
            $table->string('delivery_hours')->nullable()->change();
            $table->string('tags')->nullable()->change();

            DB::statement('ALTER TABLE `restaurant_profile` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP');
            DB::statement('ALTER TABLE `restaurant_profile` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
