<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('agent_name', 256);
            $table->enum('subscription', ['1 year', '2 years', 'monthly'])->default('1 year');
            $table->string('restaurant_name', 256);
            $table->string('manager_name', 256);
            $table->string('manager_email', 256);
            $table->string('delivery_email', 256);
            $table->integer('cuisine_type');
            $table->string('phone_number', 20);
            $table->string('phone_number2', 20);
            $table->float('sales_tax');
            $table->text('working_hours');
            $table->text('away_hours');
            $table->text('pickup_hours');
            $table->text('delivery_hours');
            $table->string('address', 256);
            $table->string('zipcode', 5);
            $table->text('tags');
            $table->enum('status', ['active', 'blocked'])->default('active');
            $table->integer('page_id')->nullable();
            $table->integer('menu_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_profile');
    }
}
