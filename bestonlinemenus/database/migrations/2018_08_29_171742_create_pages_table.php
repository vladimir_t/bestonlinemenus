<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 256)->nullable();
            $table->string('menu_page_name', 256)->nullable();
            $table->enum('nav_type', ['left', 'top'])->default('left');
            $table->string('menu_navigation_color', 7)->nullable();
            $table->string('menu_background_color', 7)->nullable();
            $table->string('logo', 256)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
