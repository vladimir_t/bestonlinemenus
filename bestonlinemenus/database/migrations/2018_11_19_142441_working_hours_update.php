<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkingHoursUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_hours_relation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id');
            $table->enum('type', ['working', 'away', 'delivery', 'pickup'])->default('working');
            $table->enum('day', ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'])->default('Mon');
            $table->string('start_date', 30)->nullable();
            $table->string('end_date', 30)->nullable();
            $table->enum('isClosed', ['true', 'false'])->default('false');

            $table->unique(['restaurant_id', 'type', 'day'], 'unique');
        });

        Schema::table('restaurant_profile', function(Blueprint $table)
        {
            $table->dropColumn('working_hours');
            $table->dropColumn('away_hours');
            $table->dropColumn('pickup_hours');
            $table->dropColumn('delivery_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_hours_relation');

        Schema::table(' restaurant_profile', function(Blueprint $table)
        {
            $table->text('working_hours')->nullable()->after('sales_tax');
            $table->text('away_hours')->nullable()->after('working_hours');
            $table->text('pickup_hours')->nullable()->after('away_hours');
            $table->text('delivery_hours')->nullable()->after('pickup_hours');
        });
    }
}
