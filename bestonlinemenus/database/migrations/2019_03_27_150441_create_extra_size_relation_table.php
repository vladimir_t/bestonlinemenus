<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraSizeRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extra', function (Blueprint $table) {
            $table->dropColumn('size_id');
        });

        Schema::create('extra_size_relation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('extra_id')->unsigned();
            $table->integer('size_id')->unsigned();
        });

        Schema::table('extra_size_relation', function (Blueprint $table) {
            DB::statement('ALTER TABLE `extra_size_relation` ADD FOREIGN KEY (`extra_id`) REFERENCES `extra`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
            DB::statement('ALTER TABLE `extra_size_relation` ADD FOREIGN KEY (`size_id`) REFERENCES `size`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_size_relation');

        Schema::table('extra', function (Blueprint $table) {
            $table->integer('size_id')->nullable()->after('price');
        });
    }
}
