<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesAgentsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_agents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id');
            $table->string('agent_name', 256)->unique();
        });

        Schema::table('restaurant_profile', function(Blueprint $table)
        {
            $table->dropColumn('agent_name');
            $table->float('revenue')->nullable()->after('sales_tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_agents');

        Schema::table('restaurant_profile', function(Blueprint $table)
        {
            $table->string('agent_name')->nullable()->after('user_id');
            $table->dropColumn('revenue');
        });
    }
}
