import React from 'react'
import {connect} from 'react-redux'
import { BrowserRouter as Router, Match, Route, Redirect} from 'react-router-dom'
// import {Route, Redirect} from 'react-router'
import Adminmain from "../components/Admin/AdminMain";


const PrivateRoute = ({component: Component,isAuthenticated, ...rest}) => (
    <Router>
        <Route {...rest} render={props => (
            isAuthenticated ? (
                <Adminmain />
            ) : (
                <Redirect to={{
                    pathname: '/login',
                    state: {from: props.location}
                }}/>
            )
        )}/>
    </Router>
);


const mapStateToProps = (state) => {
    return {
        isAuthenticated : state.Auth.isAuthenticated,
    }
};

export default connect(mapStateToProps)(PrivateRoute);