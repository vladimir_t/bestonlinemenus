import React from 'react'
import { BrowserRouter as Router, Match, Route} from 'react-router-dom'
import Main from '../components/Main'


const PublicRoute = ({component: Component, ...rest}) => (
    <Router>
        <Route {...rest} render={props => (
                <Main>
                    <Component {...props}/>
                </Main>
        )}/>
    </Router>
);


export default PublicRoute;