import Home from '../components/Client/Welcome'
import Login from '../components/Client/Login'
// import Register from '../components/Client/Registration'
import ForgotPassword from '../components/Client/forgotPassword'
import ResetPassword from '../components/Client/resetPassword'
import Dashboard from '../components/Admin/AdminMain'
// import NoMatch from '../pages/noMatch'

const routes = [
    {
        path: '/',
        exact: true,
        auth: false,
        component: Home
    },
    {
        path: '/login',
        exact: true,
        auth: false,
        component: Login
    },
    // {
    //     path: '/register',
    //     exact: true,
    //     auth: false,
    //     component: Register
    // },
    {
        path: '/forgot-password',
        exact: true,
        auth: false,
        component: ForgotPassword
    },
    {
        path: '/reset-password/:token/:email',
        exact: true,
        auth: false,
        component: ResetPassword
    },
    {
        path: '/dashboard',
        exact: true,
        auth: true,
        component: Dashboard
    },
    {
        path: '/home',
        exact: true,
        auth: true,
        component: Dashboard
    },
    {
        path: '/restaurants',
        exact: true,
        auth: true,
        component: Dashboard
    },
    // {
    //     path: '',
    //     exact: true,
    //     auth: false,
    //     component: NoMatch
    // }
];

export default routes;