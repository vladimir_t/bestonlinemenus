@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Page Designer</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#">Set Up</a>
                        </div>
                        <div class="col-md-6">
                            <a href="#">Preview</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger" id="alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br />
                            @endforeach
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <form method="post" action="{{'/designer/save'}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" id="id" value="@if(isset($page_info->id)){{$page_info->id}}@else{{0}}@endif" />
                                <label>Please enter your URL name:</label>
                                <input type="input" class="form-control" name="url_name" value="@if(isset($page_info->url_name)){{$page_info->url_name}}@endif" />
                                <br />
                                <label>Please enter your Menu Page name:</label>
                                <input type="input" class="form-control" name="page_name" value="@if(isset($page_info->page_name)){{$page_info->page_name}}@endif" />
                                <br />
                                <label>Please select your Menu navigation type:</label>
                                <br />
                                <input type="radio" value="left" name="nav_type" @if(isset($page_info->nav_type) && $page_info->nav_type == "left") checked ="checked" @endif  /> Left
                                <br />
                                <input type="radio" value="top" name="nav_type" @if(isset($page_info->nav_type) && $page_info->nav_type == "top") checked ="checked" @endif /> Top
                                <br />
                                <br />
                                <label>Please enter your Menu Navigation color:</label>
                                <input type="input" class="form-control" name="nav_color" value="@if(isset($page_info->nav_color)){{$page_info->nav_color}}@endif" />
                                <br />
                                <label>Please enter your Menu background color:</label>
                                <input type="input" class="form-control" name="background_color" value="@if(isset($page_info->background_color)){{$page_info->background_color}}@endif" />
                                <br />
                                <label>Please upload your Restaurant logo:</label>
                                <br />
                                @if(isset($page_info->logo))
                                    <div style="float:left;">
                                        <img src="{{$page_info->logo}}" width="200px" />
                                    </div>
                                @endif
                                <input type="file" name="logo" />
                                <br />
                                <input type="submit" class="btn btn-primary" value="Save changes and Go to Preview" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
