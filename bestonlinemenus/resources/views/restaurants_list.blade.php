@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Restaurants</h3>
                        </div>
                        <div class="col-md-6" style="text-align: right; padding-top: 15px">
                            <a href="{{'restaurant/add'}}" class="btn btn-primary">Add New</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if(!empty($profiles))
                        <table style="width: 100%">
                            <tr>
                                <th>Name</th>
                                <th>Revenue</th>
                                <th>Subscription</th>
                                <th>Delivery Method</th>
                                <th>Onboarded</th>
                                <th>Sales Agent</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($profiles as $profile)
                                <tr style="border-bottom: 1px solid">
                                    <td>{{$profile->restaurant_name}}</td>
                                    <td></td>
                                    <td>{{$profile->subscription}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$profile->agent_name}}</td>
                                    <td>
                                        <a href="{{'restaurant/edit/' . $profile->user_id}}">Edit</a><br />
                                        @if($profile->menu_id)
                                            <a href="{{'menu/edit/' . $profile->menu_id}}">Edit Menu</a><br />
                                        @endif
                                        <a href="{{'restaurant/block?id=' . $profile->id . '&status=' . $profile->status}}">
                                            @if($profile->status == 'active')
                                                Block
                                            @else
                                                Activate
                                            @endif
                                        </a><br />
                                        <a href="{{'restaurant/delete?id=' . $profile->user_id}}">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <span>Sorry, you don't have any restaurant</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
