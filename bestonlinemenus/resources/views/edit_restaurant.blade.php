@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Restaurant</h3>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="alert alert-danger" id="alert-danger" style="display:none"></div>
                    <div class="row">
                        <div class="col-md-12">
                        <input type="hidden" id="profile_id"  value="@if(isset($profile->id)){{$profile->id}}@else{{0}}@endif" />
                        <form action="{{'/restaurant/save'}}" id="save_profile" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control form-control-danger" name="agent_name" id="agent_name" placeholder="Sales Agent Name" @if(isset($profile->agent_name)) value="{{$profile->agent_name}}" @endif /><br />
                                    <input type="text" class="form-control" name="restaurant_name" id="restaurant_name" placeholder="Restaurant Name" @if(isset($profile->restaurant_name)) value="{{$profile->restaurant_name}}" @endif /><br />
                                    <input type="text" class="form-control" name="manager_name" id="manager_name" placeholder="Restaurant Manager's Name" @if(isset($profile->manager_name)) value="{{$profile->manager_name}}" @endif /><br />
                                    <input type="text" class="form-control" name="manager_email" id="manager_email" placeholder="Restaurant Manager's Email" @if(isset($profile->manager_email)) value="{{$profile->manager_email}}" @endif /><br />
                                    <input type="text" class="form-control" name="delivery_email" id="delivery_email" placeholder="Restaurant Delivery Email" @if(isset($profile->delivery_email)) value="{{$profile->delivery_email}}" @endif /><br />
                                    <select class="form-control" id="cuisine_type" name="cuisine_type">
                                        <option value="">Select cusine</option>
                                        @foreach($cuisine_list as $cuisine)
                                            <option value="{{$cuisine->id}}" @if(isset($profile->cuisine_type) &&  $profile->cuisine_type == $cuisine->id) selected="selected" @endif >{{$cuisine->cuisine_name}}</option>
                                        @endforeach
                                    </select>
                                    <br />
                                    <input type="phone" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" @if(isset($profile->phone_number)) value="{{$profile->phone_number}}" @endif /><br />
                                    <input type="phone" class="form-control" name="phone_number2" id="phone_number2" placeholder="Phone Number 2" @if(isset($profile->phone_number2)) value="{{$profile->phone_number2}}" @endif /><br />
                                    <input type="text" class="form-control" name="working_hours" id="working_hours" placeholder="Set Working Hours" @if(isset($profile->working_hours)) value="{{$profile->working_hours}}" @endif /><br />
                                    <input type="text" class="form-control" name="sales_tax" id="sales_tax" placeholder="Sales tax" @if(isset($profile->sales_tax)) value="{{$profile->sales_tax}}" @endif /><br />
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control" id="subscription" name="subscription">
                                        <option value="">Subscription</option>
                                        <option value="1 year" @if(isset($profile->subscription) && $profile->subscription  == '1 year') selected="selected" @endif >1 Year</option>
                                        <option value="2 years" @if(isset($profile->subscription) && $profile->subscription  == '2 years') selected="selected" @endif >2 Years</option>
                                        <option value="USD" @if(isset($profile->subscription) && $profile->subscription  == 'USD') selected="selected" @endif >USD</option>
                                    </select>
                                    <br />
                                    <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Enter Zipcode"  @if(isset($profile->zipcode)) value="{{$profile->zipcode}}" @endif /><br />
                                    <textarea name="address" id="address" class="form-control" placeholder="Enter the address manually"> @if(isset($profile->address)) {{$profile->address}} @endif</textarea><br />
                                    <textarea name="tags" id="tags" class="form-control" placeholder="Add keywords, like your best dishes and services"> @if(isset($profile->tags)) {{$profile->tags}} @endif</textarea><br />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-6 col-md-6" style="text-align: right">
                                    <a href="{{route('restaurants')}}" class="btn btn-default">Cancel</a>
                                    <a href="#" onclick="submitForm(event, 'close', {{$isRestaurant}})" class="btn btn-primary">Save and Close</a>
                                    @if(!$isRestaurant)
                                        <br /><br />
                                        <a href="#" onclick="submitForm(event, 'new', {{$isRestaurant}})" class="btn btn-primary">Save and Add Another Restaurant</a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        function submitForm(e, state, isRestaurant){
            e.preventDefault();
            var id = $('#profile_id').val();
            var frm = $('#save_profile');
            var data = frm.serialize();
            manager_email =  $('#manager_email').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: frm.attr("action"),
                type: 'post',
                data: {id: id, data:data},
                success: function (data) {
                    var resp = data.responseJSON;
                    if(typeof resp === 'undefined'){
                        resp = JSON.parse(data);
                    }

                    console.log(resp.status);
                    console.log(isRestaurant);

                    if (resp.status == 'success' && isRestaurant == 1) {
                        window.location.replace("/restaurant");
                        return;
                    }

                    if (resp.status == 'success' && state == 'new') {
                        window.location.replace("/restaurant/add");
                    }

                    if (resp.status == 'success' && state == 'close') {
                        window.location.replace("/restaurants");
                    }
                },
                error: function(data){
                    var errors = data.responseJSON;
                    $("#alert-danger").html("");
                    $.each(errors, function(key, value){
                        $('.alert-danger').show();

                        if(key === "errors"){
                            $.each(value, function(k, val){
                                $('.alert-danger').append('<p>'+val+'</p>');
                            });
                        }else{
                            $('.alert-danger').append('<p>'+value+'</p>');
                        }
                    });
                }
            });
        }
</script>
@endsection
