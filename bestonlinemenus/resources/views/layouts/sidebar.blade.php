<div class="col-md-2">
    <div class="sidebar">
        <ul class="sidebar_items">
            @role('admin')
            <li>
                <a href="{{ route('restaurants') }}">Restaurants</a>
            </li>
            <li>
                <a href="{{ route('menus') }}">Menus</a>
            </li>
            <li>
                <a href="{{ route('settings') }}">Settings</a>
            </li>
            @endrole

            @role('restaurant')
            <li>
                <a href="#">My Orders</a>
            </li>
            <li>
                <a href="{{ route('restaurant') }}">Restaurant Info</a>
            </li>
            <li>
                <a href="{{ route('designer') }}">Page Designer</a>
            </li>
            <li>
                <a href="{{ route('menus') }}">Menu Manager</a>
            </li>
            <li>
                <a href="#">Tutorial</a>
            </li>
            @endrole

            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</div>
