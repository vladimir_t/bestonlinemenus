<div id="menu_item_form">
    <form action="{{"/menu/save-item"}}" method="post" id="add_item" name="add_item" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="menu_id" value="@if(isset($menu_id)){{$menu_id}}@else{{0}}@endif" />
        <input type="hidden" name="item_id" value="@if(isset($item_info->id)){{$item_info->id}}@else{{0}}@endif" />
        <input type="text" class="form-control" name="name" id="item_name" placeholder="Item name" value="@if(isset($item_info->name)) {{$item_info->name}} @endif" /><br />
        <input type="text" class="form-control" name="price" id="item_price" placeholder="Item price" value="@if(isset($item_info->price)) {{$item_info->price}} @endif" /><br />
        <select class="form-control" id="section_id" name="section_id">
            <option value="">Select section</option>
            @foreach($sections as $key => $section)
                <option @if(isset($item_info->section_id) && $item_info->section_id == $key) selected="selected" @endif value="{{$key}}">{{$section}}</option>
            @endforeach
        </select>
        <br />
        <div class="item-picture">
            @if(isset($item_info->picture))
                <div style="float:left;">
                    <img src="{{$item_info->picture}}" width="200px" />
                </div>
            @endif
            <input type="file" name="picture" id="picture" />
        </div>
        <br />
        <input type="text" class="form-control" name="order" id="item_order" placeholder="Item order" value="@if(isset($item_info->order)) {{$item_info->order}} @endif" /><br />
        <textarea class="form-control" name="description" id="item_description" placeholder="Item description">@if(isset($item_info->description)) {{$item_info->description}} @endif</textarea>
        <br />
        <input type="submit" id="item_submit" class="btn-primary btn" value="@if(isset($item_info) && !empty($item_info)) Update @else Add Item @endif" />
    </form>
</div>