<div id="menu_section_form">
    <form action="{{"/menu/save-section"}}" method="post" id="add_section" name="add_section">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="menu_id" value="@if(isset($menu->id)){{$menu->id}}@else{{0}}@endif" />
        <input type="text" class="form-control" name="name" id="section_name" placeholder="Section name" /><br />
        <input type="text" class="form-control" name="order" id="section_order" placeholder="Section order" /><br />
        <textarea class="form-control" name="description" id="section_description" placeholder="Section description"></textarea>
        <br />
        <input type="submit" id="section_submit" class="btn-primary btn" value="Add Section" />
    </form>
</div>