<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Best Online Menus</title>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        Hi,{{$name}},<br />
        We've created your account for Best Online Menus.<br />
        Please use the following information to log in:<br />
        login:{{$email}}<br />
        <br />
        <br />
        You can set your password at any time on the log in page <a href="{{$reset_link . $token}}">Reset your password</a><br />
        Best regards,<br />
        Best Online Menus Team
    </div>
</div>
</body>
</html>
