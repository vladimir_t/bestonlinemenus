<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Best Online Menus</title>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        Hi, {{$name}}<br />
        Your restaurant {{$restaurant_name}} have new order on Best Online Menus!<br />
        Please check your admin panel!<br />

        <br />
        Best regards,<br />
        Best Online Menus Team
    </div>
</div>
</body>
</html>
