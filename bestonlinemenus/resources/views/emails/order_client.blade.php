<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Best Online Menus</title>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        Hi, {{$client_name}}<br />
        You order on Best Online Menus was be placed successfully!<br />
        <b>Restaurant name:</b> {{$restaurant_name}}<br />
        <b>Order info:</b><br />
        @foreach ($products as $product)
            <b>Dish name:</b> {{ $product->name }}<br />
            <b>Dish price:</b> ${{ $product->selectedSize['price'] ? $product->selectedSize['price'] : $product->price }} (price for 1 unit)<br />
            <b>Dish amount: </b> {{$product->amount}}<br />
            @if(isset($product->size))
                <b>Dish size:</b> {{ $product->selectedSize['name'] }}<br />
            @endif

            @if(isset($product->selectedExtras) && !empty($product->selectedExtras))
                <b>Dish Extra:</b><br />
                @foreach ($product->selectedExtras as $extra)
                    {{ $extra->name }} (x{{$extra->amount}}) - ${{$extra->price}} (price for 1 unit)<br />
                @endforeach
            @endif
            <br />
            <br />
        @endforeach
        <b>Total Order price:</b> ${{$total_price}}
        <br />
        <br />
        @if($payment_method == 'pay_now')
            <a href="{{env('FRONT_URL')}}/refund/{{$order_id}}">Refund link</a>
        @endif
        <br />
        <br />

        Best regards,<br />
        Best Online Menus Team
    </div>
</div>
</body>
</html>
