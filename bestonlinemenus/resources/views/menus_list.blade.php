@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Menus</h3>
                        </div>
                        <div class="col-md-6" style="text-align: right; padding-top: 15px">
                            <a href="{{'menu/add'}}" class="btn btn-primary">Add New</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if(!empty($menus))
                        <table style="width: 100%">
                            <tr>
                                <th>Name</th>
                                <th>Date created</th>
                                <th>Date published</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($menus as $menu)
                                <tr style="border-bottom: 1px solid">
                                    <td>{{$menu->name}}</td>
                                    <td>{{$menu->date_created}}</td>
                                    <td>{{$menu->date_published}}</td>
                                    <td>
                                        <a href="{{'menu/edit/' . $menu->id}}">Edit</a><br />
                                        <a href="{{'menu/delete?id=' . $menu->id}}">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <span>Sorry, you don't have any menus</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
