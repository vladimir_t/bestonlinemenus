@extends('layouts.app')

@section('content')
<style type="text/css">
    #menu_item_form,
    #menu_section_form{
        display: none;
    }

    .add_form{
        margin-top: 10px;
    }
</style>
<div class="container">
    <div class="row">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Menu</h3>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="alert alert-danger" id="alert-danger" style="@if(!$errors->any()) display:none @endif">
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br />
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="menu_id"  value="@if(isset($menu->id)){{$menu->id}}@else{{0}}@endif" />
                            <form action="{{'/menu/save'}}" id="save_menu" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Menu name" @if(isset($menu->name)) value="{{$menu->name}}" @endif /><br />
                                        <select class="form-control" id="restaurant_id" name="restaurant_id">
                                            <option value="">Select restaurant</option>
                                            @foreach($restaurants as $key => $restaurant)
                                                <option value="{{$key}}" @if(isset($menu->rest_id) && $menu->rest_id == $key) selected="selected" @endif >{{$restaurant}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if(!empty($menu) && $menu->id != 0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="add_form">
                                    <a href="#" onclick="showForm(event, 'menu_section_form')" class="add_link">Add Section</a>
                                    @include('layouts.menu.add_section')
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="add_form">
                                    <a href="#" onclick="showForm(event, 'menu_item_form')" class="add_link">Add Item</a>
                                    @include('layouts.menu.add_item', ['menu_id' => (!empty($menu)) ? $menu->id : 0, 'sections' => $sections])
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-offset-6 col-md-6" style="text-align: right">
                            <a href="{{route('menus')}}" class="btn btn-default">Cancel</a>
                            <a href="#" onclick="submitForm(event, 'close')" class="btn btn-primary">Save and Close</a>
                            <br /><br />
                            <a href="#" class="btn btn-primary">Publish updated menu</a>
                        </div>
                    </div>
                    @if(isset($menu_sections) && !empty($menu_sections))
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($menu_sections as $key => $items)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>{{$key}}</h3>
                                            @if(!empty($items))
                                                @foreach($items as $item)
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h4>{{$item['item_name']}}</h4>
                                                        </div>
                                                        <div class="col-md-6" style="text-align: right">
                                                            <a href="{{"/menu/edit-item/" . $item['item_id']}}">Edit</a>
                                                            <a href="{{"/menu/delete-item/" . $menu->id . "/" . $item['item_id']}}">Delete</a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <span>Price: @if(isset($item['item_price'])) {{$item['item_price']}} @else {{"0.00"}} @endif</span>
                                                            <p>
                                                                {{$item['item_description']}}
                                                            </p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            @if(isset($item['item_picture']))
                                                                <img src="{{$item['item_picture']}}" width="200px" />
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        function submitForm(e, state){
            e.preventDefault();
            var id = $('#menu_id').val();
            var frm = $('#save_menu');
            var data = frm.serialize();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: frm.attr("action"),
                type: 'post',
                data: {id: id, data:data},
                success: function (data) {
                    var resp = data.responseJSON;
                    if(resp.status == 'success' && state == 'close'){
                        window.location.replace("/menus");
                    }
                },
                error: function(data){
                    var errors = data.responseJSON;
                    $("#alert-danger").html("");
                    $.each(errors, function(key, value){
                        $('.alert-danger').show();

                        if(key === "errors"){
                            $.each(value, function(k, val){
                                $('.alert-danger').append('<p>'+val+'</p>');
                            });
                        }else{
                            $('.alert-danger').append('<p>'+value+'</p>');
                        }
                    });
                }
            });
        }

        function showForm(e, form_id) {
            e.preventDefault();
            var form_div = document.getElementById(form_id);
            if (form_div.style.display === "none" || form_div.style.display === "") {
                form_div.style.display = "block";
            } else {
                form_div.style.display = "none";
            }
        }
</script>
@endsection
