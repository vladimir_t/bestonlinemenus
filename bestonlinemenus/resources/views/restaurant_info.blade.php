@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Restaurant Info</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="#">Contact Information Settings</a>
                        </div>
                        <div class="col-md-4">
                            <a href="#">Billing Information</a>
                        </div>
                        <div class="col-md-4">
                            <a href="#">Peyment Information</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if(!empty($restaurant))
                        <div class="row">
                            <div class="col-md-6">
                                <p>{{$restaurant->restaurant_name}}</p>
                                <p>{{$restaurant->manager_name}}</p>
                                <p>{{$restaurant->manager_email}}</p>
                                <p>{{$restaurant->delivery_email}}</p>
                                <p>{{$restaurant->phone_number}}</p>
                                <p>Working hours:</p>
                                <p>Away time:</p>
                                <p>Pickup hours:</p>
                                <p>{{$restaurant->address}}</p>
                                <p><a href="{{'restaurant/edit/' . $restaurant->user_id}}" class="button btn btn-primary">Edit</a></p>
                            </div>
                            <div class="col-md-6">
                                <p>Delivery Service</p>
                                <p>
                                    <label>Estimated Pickup Time</label>
                                    <input type="input" /> mins
                                </p>
                                <p>
                                    <label>Estimated Delivery Time</label>
                                    <input type="input" /> mins
                                </p>
                                <p>Order Notification Type</p>
                                <p>
                                    <input type="checkbox" /> email
                                </p>
                                <p>
                                    <input type="checkbox" /> sound
                                </p>
                                <p>Connected Accounts</p>
                                <p>
                                    <input type="button" class="btn btn-primary" value="Yelp" />
                                </p>
                                <p>
                                    <input type="button" class="btn btn-primary" value="Foursquare" />
                                </p>
                            </div>
                        </div>
                    @else
                        <span>Sorry, you don't have any restaurant</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
