<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Best Online Menus | @if(isset($restaurant['restaurant'])){{$restaurant['restaurant']->restaurant_name}}@endif</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                display: block;
                width: 1200px;
                margin-left: 35px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .res-name{
                text-align: left;
                font-size: 24px;
            }

            .sections-menu-wrap{
                background: @if(isset($page_info->nav_color)) {{$page_info->nav_color}} @else none @endif;
            }

            .sections-menu{
                list-style: none;
                padding-left: 0px;
            }
            
            .sections-menu-top li{
                position:relative;
                display: inline-block;
                padding-right: 20px;
                margin-bottom: 20px;
            }

            .sections-menu-left{
                margin-top: 0px;
                margin-right: 20px;
                padding:20px;
            }

            .left{
                float: left;
            }

            .items-list{
                padding: 20px;
                background: @if(isset($page_info->background_color)) {{$page_info->background_color}} @else none @endif;
                width: @if(isset($page_info->nav_type) && $page_info->nav_type == "left") 80% @else 100% @endif;
            }

            .item-column-left,
            .item-column-right{
                display:block;
                width: 10%;
                padding: 0 20px;
            }

            .item-column-center{
                display: block;
                width: 60%;
            }

            .item-column-center h4{
                margin-top:0px;
            }

            .item-price{
                margin-top: 50px;
            }

            .item-picture{
                padding: 0 20px;
            }

            .item-picture img{
                width: 300px;
            }

            .menu-item-content{
                margin: 20px 0;
            }
        </style>
    </head>
    <body>
        <div class="full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            @if(isset($restaurant['restaurant']) && $restaurant['restaurant']->status == 'active')
                <div class="content">
                    @if(isset($page_info->logo))
                        <div class="row">
                            <div class="col-md-12 m-b-md restaurant-logo">
                                <img src="{{$page_info->logo}}" alt="Restaurant Logo" />
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12 m-b-md res-name">
                            {{$restaurant['restaurant']->restaurant_name}}
                        </div>
                    </div>

                    @if(isset($restaurant['sections']))
                        <div class="row">
                            <div class="col-md-12 m-b-md res-name">
                                @if(isset($page_info->nav_type) && $page_info->nav_type == "top")
                                    <div class="row">
                                        <div class="col-md-12 sections-menu-wrap">
                                            <ul class="sections-menu sections-menu-top">
                                                @foreach($restaurant['sections'] as $section)
                                                    <li><a href="#{{$section['id']}}">{{$section['name']}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    @if(!isset($page_info->nav_type) || isset($page_info->nav_type) && $page_info->nav_type == "left")
                                        <div class="col-md-4 left sections-menu-wrap">
                                            <ul class="sections-menu sections-menu-left">
                                                @foreach($restaurant['sections'] as $section)
                                                    <li><a href="#{{$section['id']}}">{{$section['name']}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="@if(isset($page_info->nav_type) && $page_info->nav_type == "left") col-md-8 @else col-md-12 @endif items-list left">
                                        @foreach($restaurant['sections'] as $section)
                                            <div class="section-wrapper" id="{{$section['id']}}">
                                                <h3>{{$section['name']}}</h3>
                                                <p class="section-description">{{$section['description']}}</p>
                                                <div class="items-wrapper">
                                                    @foreach($section['items'] as $item)
                                                        <div class="menu-item-content">
                                                            <div class="item-column-left left">
                                                                <div class="add-item-link">Add link here</div>
                                                                @if($item->price)
                                                                    <div class="item-price">${{$item->price}}</div>
                                                                @endif
                                                            </div>
                                                            <div class="item-column-center left">
                                                                <h4>{{$item->name}}</h4>
                                                                <p>{{$item->description}}</p>
                                                            </div>
                                                            <div class="item-column-right item-picture left">
                                                                <img src="{{$item->picture}}">
                                                            </div>
                                                        </div>
                                                        <div style="clear: both"></div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @else
                <div class="content">
                    <div class="m-b-md">
                        <h2>Sorry, this restaurant was blocked. Please contact admin!</h2>
                    </div>
                </div>
            @endif
        </div>
    </body>
</html>
