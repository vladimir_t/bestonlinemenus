<?php
return [
    'public_id' => env('STRIPE_PUBLIC_ID'),
    'secret_id' => env('STRIPE_SECRET_ID'),
];